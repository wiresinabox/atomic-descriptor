import numpy as np
import scipy.special as spf
from scipy.special import spherical_jn as sjn
import warnings



def fnl(r, rc, n, l, unl=None, unpl=None):
    """A linear combination of bessel functions, see Appendix A
    Where l is the bessel function order"""
    #Since the spherical bessel function is just a multiplier of a bessel function of the first kind
    #Then the zeros should be the same.
    #coefficients so fnl(rc)=0
    if isinstance(unl, type(None)) and isinstance(unpl, type(None)):
        unml, unl, unpl = [root for root in spf.jn_zeros(l, n+5) if root != 0][n-1: n+2] #nth and n+1st nonzero root
    anl = (unpl/sjn(l+1, unl))
    bnl = -(unl/sjn(l+1, unpl))
    #normalization constant
    cnl = ((1/rc**3)*(2/((unl**2)+(unpl**2))))**(1/2)
    fnl = cnl*(anl*sjn(l, unl*(r/rc)) + bnl*sjn(l, unpl*(r/rc)))
    return fnl


def gnl(r, rc, n, l):
    """See Appendix A
       r is a numpy vecotr
       rc is a the critical cutoff radius
       n is root number used
       l is the order of the spherical Bessel functions
    """
    #r may need to become a vector from 0 to rc
    #Given the g0l(r) = f0l(r), we must construct every other gn'l before gnl
    unml, unl, unpl = [root for root in spf.jn_zeros(l, n+5) if root != 0][n: n+3] #unl is the n+1st root of jl(r)
    g = fnl(r, rc, 0, l, unl=unl, unpl=unpl)
    d=1
    for ni in range(1, n+1):
        unml, unl, unpl = [root for root in spf.jn_zeros(l, n+5) if root != 0][ni-1: ni+2] #nth and n+1st nonzero root
        enl = ((unml**2)*(unpl**2))/(((unml**2)+(unl**2))*((unl**2)+(unpl**2)))
        f = fnl(r, rc, ni, l, unl=unl, unpl=unpl)
        d = 1 - (enl/np.sqrt(d)) 
        g = (f + np.sqrt(1-d)*g)/np.sqrt(d) 
    return g


def Pml_norm(x, m, l):
    "Normalized Associated Legendre Functions of the first kind"
    coeff = (((2*l+1)/2) * (spf.factorial(l-m, exact=False)/spf.factorial(l+m, exact=False)))**(1/2)
    leg, legDiv= spf.lpmn(m, l, x)
    return coeff * leg[-1, -1]

def CGcoeff(l, m ,j1, j2, m1, m2):
    #Clebsch-Gordon coefficient
    #print('l:', l, 'm:', m, 'j1:',j1, 'j2:',j2, 'm1:', m1, 'm2:', m2)
    delta = int(m == m1+m2)
    #print('delta:', delta)
    p1 = np.sqrt((2*l+1)*spf.factorial(l + j1 - j2)*spf.factorial(l-j1+j2)*spf.factorial(j1 + j2 - l) / spf.factorial((j1 + j2 + l + 1)))
    p2 = np.sqrt(spf.factorial(l + m)*spf.factorial(l-m)*spf.factorial(j1-m1)*spf.factorial(j1+m1)*spf.factorial(j2-m2)*spf.factorial(j2+m2))
    p3 = 0
    for k in range(int(np.floor(l+m+1))): #double check what k is. I'm not entirely sure.
        div = (spf.factorial(k)*spf.factorial(j1+j2-l-k)*spf.factorial(j1-m1-k)*spf.factorial(j2+m2-k)*spf.factorial(l-j2+m1+k)*spf.factorial(l-j1-m2+k))
        #print('k', k, 'div', div)
        if div == 0: #necessary to remove div/0 errors
            pass
        else:
            p3 += (-1)**k / div

    return delta*p1*p2*p3

    

def Xi(r0, r1, r2, theta0, theta1, theta2, phi0, phi1, phi2, n0, n1, n2, l0, l1, l2, rc):
    """Something or other. See Appendix E. Only valid when l0 + l1 + l2 is even?
        l>=0 and -l <= m <= l
    """
    coeff = ((-1)**l0)/(np.sqrt(2*l0+1)) * 1/((2*np.pi)**(3/2))
    gpart = gnl(r0, rc, n0, l0) * gnl(r1, rc, n1, l1) * gnl(r2, rc, n2, l2)
    
    #I uh don't know what the summmuation with m0 m1 m2 is coming from
    #m0, m1, and m2 are magnetic spin coefficients so they're bounded by l. figure that out.
    sumpart = 0
    for m0 in np.arange(-l0, l0+0.1):
        for m1 in np.arange(-l1, l1+0.1):
            for m2 in np.arange(-l2, l2+0.1):
                p1 = CGcoeff(l0, m0, l1, l2, m1, m2) * Pml_norm(np.cos(theta0), m0, l0) * Pml_norm(np.cos(theta1), m1, l1) * Pml_norm(np.cos(theta2), m2, l2)
                p2 = np.cos(-m0*phi0+m1*phi1+m2*phi2)
                sumpart += p1*p2
                print((m0, m1, m2),m0, m1+m2, m0==m1+m2) 
                print(CGcoeff(l0, m0, l1, l2, m1, m2), p1, p2, sumpart)
                print(Pml_norm(np.cos(theta0), m0, l0), Pml_norm(np.cos(theta1), m1, l1), Pml_norm(np.cos(theta2), m2, l2))
    print('coeff:', coeff)
    print('gpart:', gpart)
    print('sumpart:', sumpart)
    return coeff*gpart*sumpart
   
