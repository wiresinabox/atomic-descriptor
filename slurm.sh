#!/bin/bash -l
#SBATCH --job-name=atomdes

# I/O
#SBATCH --output=/home/esuwandi/atomic-descriptor/job-results/ad-%j.out
#SBATCH  --error=/home/esuwandi/atomic-descriptor/job-results/ad-%j.err

# Mail stuff I guess
#SBATCH --mail-type=END
#SBATCH --mail-user=etsuwandi@ucdavis.edu

#SBATCH -t 10-00:00:00
# Number of requested nodes. 64 cores, 128 GB RAM
#SBATCH --nodes=1
#SBATCH --cpus-per-task=24
#SBATCH --partition=med
# Print Info
echo "SLURM_NODELIST = $SLURM_NODELIST"
echo "SLURM_NODE_ALIASES = $SLURM_NODE_ALIASES"
echo "SLURM_NNODES = $SLURM_NNODES"
echo "SLURM_CPUS_ON_NODE = $SLURM_CPUS_ON_NODE"
echo "SLURM_TASKS_PER_NODE = $SLURM_TASKS_PER_NODE"
echo "SLURM_NTASKS = $SLURM_NTASKS"
echo "SLURM_JOB_ID = $SLURM_JOB_ID"
echo "SLURM_ARRAY_TASK_ID = $SLURM_ARRAY_TASK_ID"

module load bio3/1.0

hostname
sdate=$(date +%m-%d-%Y)

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -q 5 -t lattice 200
##GW examples
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t peturb -d L2 -c GW 200
##2 body counter examples
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t counterPowerRef -d L2 -c G2 -seed 1 -envseed 1 100
##3 body counter examples
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t counterRef -d L2 -c G4 200
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t counterRef -std 0.01 -d L2 -c G4 200
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t counterPlaneRef -d L2 -c G4 200

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t err=peturb -d L2 -c GW 100

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t err=peturb -d L1 -c GW 200

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t errp=peturb -d L2 -c GW 100
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -t errp=peturb -d L1 -c GW 200

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -a 3 -envseed 1 -std 0.005 -t err=random -d L2 -c GW 100
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -a 4 -envseed 1 -std 0.005 -t err=random -d L2 -c GW 100
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -a 5 -envseed 1 -std 0.005 -t err=random -d L2 -c GW 100
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -a 6 -envseed 1 -std 0.005 -t err=random -d L2 -c GW 100

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID} -o /home/esuwandi/atomic-descriptor/job-results -a 3 -envseed 1 -std 0.005 -t errp=random -d L2 -c GW 100

#Figure 1: Counter Configurations
















for STD in 0 0.001 0.005 0.01 0.1
do
        if [ $STD = 0 ]
        then
                num=$SLURM_CPUS_ON_NODE
        else
                num=200
        fi

        echo $STD $num
#	srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterRef-${STD/./_} -o /home/esuwandi/atomic-descriptor/job-results -t counterRef -std $STD -short -save  -d L2 -c GW $num
#	srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterPlaneRef-${STD/./_} -o /home/esuwandi/atomic-descriptor/job-results -t counterPlaneRef -short -save -std $STD  -d L2 -c GW $num
#	srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterBispectrumRef-${STD/./_} -o /home/esuwandi/atomic-descriptor/job-results -t counterBispectrumRef -short -save -envseed 1 -std $STD -d L2 -c GW $num
done

#Figure 2 & 4: Stability Plots

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a3 -o /home/esuwandi/atomic-descriptor/job-results -a 3 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -t err=random -d L2 -c GW -short -save 200
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a4 -o /home/esuwandi/atomic-descriptor/job-results -a 4 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -t err=random -d L2 -c GW -short -save 200
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a5 -o /home/esuwandi/atomic-descriptor/job-results -a 5 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -t err=random -d L2 -c GW -short -save 200
#
#Figure 3: Smoke Plots

#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-L2 -o /home/esuwandi/atomic-descriptor/job-results -t peturb -d L2 -c GW -save 200
#srun python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-L1 -o /home/esuwandi/atomic-descriptor/job-results -t peturb -d L1 -c GW -save 200


SLURM_JOB_ID='home'
SLURM_CPUS_ON_NODE=3
outDir='./'
#
for STD in 0 0.005 0.01 0.1
do
        if [ $STD = 0 ]
        then
                num=3 #$SLURM_CPUS_ON_NODE
        else
                num=100
        fi

        echo $STD $num
#	 python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterRef-${STD/./_} -o $outDir -t counterRef -std $STD   -d L2 -c GW -save $num
#	 python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterPlaneRef-${STD/./_} -o $outDir -t counterPlaneRef -save -std $STD  -d L2 -c GW $num
	 python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-counterBispectrumRef-${STD/./_}-new -o $outDir -t counterBispectrumRef -save -envseed 1 -std $STD -d L2 -c GW -short $num
done
#
##Figure 2 & 4: Stability Plots
#
 #python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a3 -o $outDir -a 3 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -short -save -t err=random -d L2 -c GW 200
 #python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a4 -o $outDir -a 4 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -short -save -t err=random -d L2 -c GW 200
 #python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-a5 -o $outDir -a 5 -envseed 1 -std 0.005 -lo 10 -hi 1000 -num 100 -short -save -t err=random -d L2 -c GW 200
#
##Figure 3: Smoke Plots
#
# python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-L2 -o $outDir -t peturb -d L2 -c GW -save 200
# python3 main.py -n $SLURM_CPUS_ON_NODE -j ${sdate}-${SLURM_JOB_ID}-L1 -o $outDir -t peturb -d L1 -c GW -save 200

