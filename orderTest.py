import numpy as np
import descriptor_direct as desd


def get_parameters_exhaust(q, Nb=1, **kwargs):
    #Exhaustively generate every possible combination of paramters subject to the conditions to make sure we're not skipping some.
    paramList = []
    maxq = q
    for q in range(0, maxq+1):
        for n0 in range(0, q+1):
            for n1 in range(0, q+1):
                for n2 in range(0, q+1):
                    for l0 in range(0, q+1):
                        for l1 in range(0, q+1):
                            for l2 in range(0, q+1):
                                for p in range(0, Nb**2+1):
                                
                                    #Condition: sum(n) + sum(l) = q by construction of q
                                    
                                    #cond1 = n0+n1+n2+l0+l1+l2 == q
                                    
                                    cond1 = n0+n1+n2+(max(l0,l1,l2)) == q
                                    
                                    #Condition: l0+l1+l2 is even
                                    cond2 = (l0+l1+l2) % 2 == 0
                                    #Condition: CG conditon
                                    cond3 = np.abs(l1-l2) <= l0
                                    cond4 = l0 <= l1 + l2
                                    if np.all([cond1, cond2, cond3, cond4]):

                                        param = (n0, n1, n2, l0, l1, l2, p)
                                        print(q, param)
                                        paramList.append(param)
    return paramList                  


q=3
Nb=3 #Not really interested in p variations
paramSet = set([tup[1] for tup in desd.get_parameters(q, Nb)])
paramExSet = set(get_parameters_exhaust(q, Nb))

print(len(paramSet), len(paramExSet))

