import descriptor_direct as desd
import descriptor_crgw as crgw
import numpy as np
import scipy as sp
import scipy.special as spf
import matplotlib.pyplot as plt
import mputil as mpu
eps = np.finfo(float).eps
    
distL1 = lambda p1, p2: np.sum(np.abs(desd.sph2cart(p1) - desd.sph2cart(p2)))
distL2 = lambda p1, p2: np.sqrt(np.sum((desd.sph2cart(p1) - desd.sph2cart(p2))**2))
distL1cart = lambda p1, p2:   np.sum(np.abs(p1 - p2))
distL2cart = lambda p1, p2: np.sqrt(np.sum((p1 - p2)**2))

def angleL2cart(p1, p2, p3):
    #Gets angle around p2
    pc = np.zeros(3)
    b1 = p1 - p2
    b2 = p3 - p2
    return np.arccos(np.dot(b1, b2)/(distL2cart(pc, b1)*distL2cart(pc, b2)))

def gen_random_env(rc, N=1, sph=True, useLocalSeed=False, envseed=None, center=True, offsetxyz=[0,0,0], **kwargs):
    if useLocalSeed:
        rng = np.random.RandomState(envseed)
    else:
        rng = np.random
    
    points = []
    if center:
        cp = [0,0,0]
        if sph: cp = desd.cart2sph(cp)
        points.append(cp)
        N = N-1 #since this adds an atom
    for ni in range(N):
        #p = (1/np.sqrt(3))*rc*(rng.random(3)-0.5)*2 + np.array(offsetxyz) #rng.random is not uniformly distributed for a sphere.
        p=(rng.rand(3)-0.5)*2*rc 
        while np.dot(p,p) > 1:
            p=(rng.rand(3)-0.5)*2*rc 
        
        p = gen_peturb_point(p, **kwargs)
        if sph:
            p = desd.cart2sph(p)
        points.append(p)
    return points

def gen_peturb_point(point, mu=0, std=0, offsetxyz=[0,0,0], scatterRNG=None, seed=None, **kwargs):
    """peturbs a point normally"""
    if isinstance(scatterRNG, type(None)): rng = np.random.RandomState(seed)
    else: rng = scatterRNG

    p=np.array(point)+rng.normal(mu,std, size=len(point)) + np.array(offsetxyz)
    return p

def gen_peturb_env(rc, env=None, center=False, **kwargs):
    if isinstance(env, type(None)): env = [[0,0,0], [rc*0.5,0,0], [0, rc*0.5, 0]]
    penv = []
    for i in range(len(env)):
        point = env[i]
        p = gen_peturb_point(point, **kwargs)
        if center and i ==0:
            poff=p
        if center: 
            p = p-poff
        if np.sqrt(np.sum(p**2)) > rc: continue
        if 'sph' in kwargs and kwargs['sph']: p= desd.cart2sph(p)
        
        penv.append(p)
        
    return penv

def gen_lattice_env(rc, latticeCoeff=[1,1,1], latticeType='o', mu=0, std=0, sph=True, envseed=None, offsetxyz=[0,0,0], center=True, posLattice=False, **kwargs):
    """Generates a lattice with gaussion/normal offsets."""
    rng = np.random.RandomState(envseed)
    
    
    points = []
    if latticeType == 'o': #o for orthorhombic
        
        xspacing, yspacing, zspacing = latticeCoeff
        xoff = offsetxyz[0]%xspacing
        yoff = offsetxyz[1]%xspacing
        zoff = offsetxyz[2]%xspacing
        xpoints =np.append(np.arange(0, -(rc+xspacing+xoff), -xspacing)[1:], np.arange(0, 1.1*rc+xspacing+xoff, xspacing))
        ypoints =np.append(np.arange(0, -(rc+yspacing+yoff), -yspacing)[1:], np.arange(0, 1.1*rc+yspacing+yoff, yspacing))
        zpoints =np.append(np.arange(0, -(rc+zspacing+zoff), -zspacing)[1:], np.arange(0, 1.1*rc+zspacing+zoff, zspacing))
        
        p=gen_peturb_point([0,0,0], mu=mu, std=std, offsetxyz=offsetxyz, rng=rng)
        if center:
            ctrans = p
            p = p - ctrans
        if sph: p= desd.cart2sph(p)
        points.append(p)
        for x in xpoints:
            for y in ypoints:
                for z in zpoints:
                    if x == y == z == 0: continue
                    if posLattice and np.any(np.array([x,y]) < 0): continue

                    p=gen_peturb_point([x,y,z], mu=mu, std=std, offsetxyz=offsetxyz, rng=rng)
                    if center:
                        p = p - ctrans #Centers on middle atom
                    if np.dot(p,p) <= rc:
                        if sph: p= desd.cart2sph(p)
                        points.append(p)
    return points

def translate_env(env, translate=[0,0,0], sph=True, **kwargs):
    '''If sph is true, assumes sph coming in and sph coming out. translate in cart'''
    newEnv = []
    for j in range(len(env)):
        p = env[j]
        if sph: p = desd.sph2cart(p)
        p = p + np.array(translate)
        if sph: p = desd.cart2sph(p)
        newEnv.append(p)
    print(env, newEnv, translate)
    return newEnv

def rotate_env(env, rotate=[0,0,0], sph=True, **kwargs):
    '''rotate is the angle in [angle, [lmn]]'''
    Rx =np.array([[1,0,0],[0,np.cos(rotate[0]), -np.sin(rotate[0])],[0,np.sin(rotate[0]), np.cos(rotate[0])]])
    Ry = np.array([[np.cos(rotate[1]), 0, np.sin(rotate[1])], [0, 1, 0], [-np.sin(rotate[1]), 0, np.cos(rotate[1])]])
    Rz = np.array([[np.cos(rotate[2]), -np.sin(rotate[2]), 0], [np.sin(rotate[2]), np.cos(rotate[2]), 0], [0,0,1]])

    T = np.matmul(np.matmul(Rz, Ry), Rx)

    newEnv = []
    for j in range(len(env)):
        p = env[j]
        if sph: p = desd.sph2cart(p)
        #p = p + np.array(pol, azm)
        p = np.matmul(p, T)
        if sph: p = desd.cart2sph(p)
        newEnv.append(p)
    return newEnv

def invert_env(env, invert=True, sph=True, **kwargs):
    if invert:
        T = np.identity(3)*-1 
        for j in range(len(env)):
            p = env[j]
            if not sph: p = desd.cart2sph(p)
            #p = p + np.array(pol, azm)
            p = np.matmul(p, T)
            if not sph: p = desd.sph2cart(p)
            newEnv.append(p)
        return newEnv
    else:
        return env

def gen_center_env(rc, r=0.5, pol=np.pi/2, azm=0, offset = [0,0,0], sph=True, center=True, **kwargs):
    """Generates a center point and several other points given certain parameters. r, pol, azm can also be list/tuples/ndarrays for multiple points"""
    points = []
    if center==True:
        centerPoint = np.array([0,0,0]) + np.array(offset)
        points.append(centerPoint)
    numPoints = 1 #number of points
    if isinstance(r, (list, tuple, np.ndarray)): 
        if numPoints < len(r): numPoints = len(r)
    if isinstance(pol, (list, tuple, np.ndarray)): 
        if numPoints < len(pol): numPoints = len(pol)
    if isinstance(azm, (list, tuple, np.ndarray)): 
        if numPoints < len(azm): numPoints = len(azm)
    for pi in range(numPoints):
        if isinstance(r, (list, tuple, np.ndarray)): ri = r[pi]
        else: ri = r
        if isinstance(pol, (list, tuple, np.ndarray)): poli = pol[pi]
        else: poli = pol
        if isinstance(azm, (list, tuple, np.ndarray)): azmi = azm[pi]
        else: azmi = azm
        poli = poli%(np.pi) #Wrapping around
        azmi = azmi%(2*np.pi)
        p = np.array([ri, poli, azmi])
        if sph == False:
            p = desd.sph2cart(p)
        if ri <= rc:
            points.append(p)
    return points

def _xyz_conv(ax):
    if ax == 'x': return 0
    elif ax == 'y': return 1
    elif ax == 'z': return 2
    else: raise('_xyz_conv(): Unknown axis "{}"'.format(ax))

def gen_planar_counter_env(rc, rmult = 1, center=True, plane='xy', degenType = True, sph = True, **kwargs):
    """Invariant to 3-body correlations (G4, SOAP)"""
    a1 = _xyz_conv(plane[0])
    a2 = _xyz_conv(plane[1])
    points = []
    pcent = [0,0,0]
    if center: points.append(pcent)
    p1 = np.zeros(3); p1[a2]=1
    p2 = np.zeros(3); p2[a2]=-1
    p3 = np.zeros(3)
    p3[a2] = -1; p3[a1] = 1
    p3 = p3/distL2cart(pcent, p3)
    p4 = np.zeros(3)
    p4[a2] = 1; p4[a1] = 1
    p4 = p4/distL2cart(pcent, p4)
    p4p = np.zeros(3)
    p4p[a2] = -1; p4p[a1] = -1
    p4p = p4p/distL2cart(pcent, p4p)
    
    points.append(p1)
    points.append(p2)
    points.append(p3)
    if isinstance(degenType, str) and degenType == 'both':
        points.append(p4)
        points.append(p4p)
    elif degenType: 
        points.append(p4)
    else: 
        points.append(p4p)
    
    for i in range(len(points)):
        p = points[i]
        p = gen_peturb_point(p, **kwargs)
        if i == 0: poff = p
        if center: p = p-poff
        if sph: p = desd.cart2sph(p)
        points[i] = p
    
    return points
   
def gen_multiplanar_counter_env(rc, degenType=(True, True), **kwargs):
    """Generates high multiplicity degenerate environments"""
    points = []
    tkwargs = kwargs.copy()
    tkwargs['plane'] = 'xz'
    tkwargs['degenType'] = degenType[0]
    points.extend(gen_planar_counter_env(rc, **tkwargs))
    tkwargs['plane'] = 'yz'
    tkwargs['degenType'] = degenType[1]
    tkwargs['center'] = False 
    points.extend([point for point in gen_planar_counter_env(rc, **tkwargs) if np.any(np.all(point != np.array(points), axis=1))])
    return points

def gen_counter_env(rc, ax=1, az=1, bx=1, by=1, bz=1, cy=1, cz=1, degenType = 'both', sph=True, center=True, **kwargs):
    """Creates 3-body degenerate environments (Invariant to G4, SOAP?) as defined by S. Pozdnyakov et al. On the Completeness of Atomic Structure Representations
    degenType = True, defines which direction the C point is pointing. Switch to false to create a degenerate environment. 'both' will add both points.
    """
    points = []
    l = rc/np.sqrt(3)
    points.append([0, 0, 0]) #center
    points.append([l*ax, 0, l*az]) #a
    points.append([l*bx, l*by, l*bz]) #b
    points.append([l*-bx, l*-by, l*bz]) #b'
    if isinstance(degenType, str) and degenType == 'both':
        points.append([0, l*cy, l*cz]) #c, x+
        points.append([0, l*-cy, l*cz]) #c, x-
    elif degenType: points.append([0, l*cy, l*cz]) #c, x+
    else: points.append([0, l*-cy, l*cz]) #c, x-
    
    for i in range(len(points)):
        p = points[i]
        p = gen_peturb_point(p, **kwargs)
        if i == 0: poff = p
        if center: p = p-poff
        if sph: p = desd.cart2sph(p)
        points[i] = p

    return points


def gen_power_counter_env(rc, rmult=1, base = 'standard', noBisect=1, degenType ='both', envseed=None, sph=True, center=True, **kwargs):
    """Constructs 2 environments where all distance angle triplets of pairs of points (r1, r2, theta) are the same except for r+ and r-. This creates identical origin-centerd power spectra."""
    #Create two points that are the same distance to the origin but are not parallel or antiparallel.
    rng = np.random.RandomState(envseed)
    randPoint = lambda n : 2*(rng.rand(n)-0.5)
    points = []
    pcent = [0,0,0]
    if center: points.append(pcent)
    if base =='random': 
        while True:
            prp = rmult*rc*randPoint(3)
            prn = rmult*rc*randPoint(3)
            if np.abs(prp) != np.abs(prn):
                break
    else: 
        prp = np.array([0,rc,rc])
        prp = prp/distL2cart(pcent, prp)
        prn = np.array([0,-rc,rc])
        prn = prn/distL2cart(pcent, prn)
    if isinstance(degenType, str) and degenType == 'both':
        points.append(prp)
        points.append(prn)
    elif degenType: points.append(prp)
    else: points.append(prn)
    
    #define orthonormal base
    ih=(prp + prn) / distL2cart(pcent, prp + prn)
    jh=(prp - prn) / distL2cart(pcent, prp - prn)
    kh=np.cross(prp, prn) / distL2cart(pcent, np.cross(prp, prn))
    bisect = np.zeros((3,3))
    basis = np.zeros((3,3))
    bisect[0,:] = ih
    #bisect[1,:] = jh
    bisect[2,:] = kh
    
    basis[0,:] = ih
    basis[1,:] = jh
    basis[2,:] = kh
    #print(bisect)
    for i in range(noBisect): #Number of points on the bisecting plane between r+ and r-
        while True:
            p=np.matmul(rc*randPoint(3),bisect)
            if distL2cart(pcent, p) <= rc: break
        points.append(p)

    #create pairs of points outside of the bisecting plane
    while True:
        pro = rc*randPoint(3) #point, r, outside bisecting
        if distL2cart(pcent, pro) <= rc and np.matmul(pro, np.linalg.inv(basis))[1] != 0: break #Ensures pr isn't in the bisecting plane.
    prop = pro*np.array([1, -1, 1]) #point r'. technically r' = ai -bj+ +-ck
    points.append(pro)
    points.append(prop)
    
    for i in range(len(points)):
        p = points[i]
        p = gen_peturb_point(p, **kwargs)
        if i == 0: poff = p
        if center: p = p-poff
        if sph: p = desd.cart2sph(p)
        points[i] = p

    return points
    
def gen_bispectrum_counter_env(rc, rmult=1, planermult=0.3, planePoints = 3, degenType = True, sph=True, envseed=None, center=True, azmList = [0, 60, 245], rot='random', **kwargs):
    """Generates two environments which have the same set of tetrahedra but nonsuperimposable configurations. Centered around the bispectrum """
   
    rng = np.random.RandomState(envseed)
    points = []
    pcent = desd.cart2sph([0,0,0]) #0 
    points.append(pcent)

    prp = desd.cart2sph([0,0,1])
    prn = desd.cart2sph([0,0,-1]) #p1
    if isinstance(degenType, str) and degenType == 'both':
        points.append(prp)
        points.append(prn)
    elif degenType: points.append(prp)
    else: 
        points.append(prn)
    if rot == 'random':
        rot =  rng.rand()*2*np.pi
     
    
    for i in range(planePoints):
        #azm = rng.rand()*2*np.pi
        azm = np.deg2rad(azmList[i])
        p1 = np.array([rc, np.pi/2 + np.arcsin(planermult/rc), (azm)%(2*np.pi)]) #ABC
        p2 = np.array([rc, np.pi/2 - np.arcsin(planermult/rc), (azm+rot)%(2*np.pi) ]) #DEF #Double check if the polygon needs to pass through the axis 
        points.append(p1)
        points.append(p2)
    
    for i in range(len(points)):
        p = points[i]
        p = gen_peturb_point(p, **kwargs)
        if i == 0: poff = p; 
        if center: p = p-poff
        if not sph: p = desd.sph2cart(p)
        points[i] = p
    return points

def _triangle_stats(env, i,j,k):
    pi = desd.sph2cart(env[i])
    pj = desd.sph2cart(env[j])
    pk = desd.sph2cart(env[k])
    ai = np.rad2deg(angleL2cart(pj, pi, pk))
    aj = np.rad2deg(angleL2cart(pi, pj, pk))
    ak = np.rad2deg(angleL2cart(pj, pk, pi))
    return [ai, aj, ak]

def get_env_stats(env, center=0, verbose=False):
    #Returns the ordered list of interatomic distances and angles just to double check I didn't muck something up
    distMatrix = np.zeros((len(env), len(env)))
    angleMatrix = np.zeros((len(env), len(env)))
    tetraDict = dict() #ijk (+center) : (sorted(lenghts), sorted(angles))
    for i in range(0, len(env)):
        for j in range(0, len(env)):
            pi = desd.sph2cart(env[i])
            pj = desd.sph2cart(env[j])
            dij = distL2cart(pi, pj)
            distMatrix[i,j] = dij
            aij = angleL2cart(pi, env[0], pj)
            if i != 0 and j != 0 and i != j:
                angleMatrix[i,j] = np.rad2deg(aij)
    
    pc = env[center]
    for i in range(0, len(env)):
        for j in range(0, len(env)):
            for k in range(0, len(env)):
                if not any([i==j, j==k, k==i, center in (i,j,k)]):
                    pi = desd.sph2cart(env[i])
                    pj = desd.sph2cart(env[j])
                    pk = desd.sph2cart(env[k])
                    di = distL2cart(pc, pi)
                    dj = distL2cart(pc, pj)
                    dk = distL2cart(pc, pk)
                    dij = distL2cart(pi, pj)
                    djk = distL2cart(pj, pk)
                    dik = distL2cart(pi, pk)
                    tetraDists = [di, dj, dk, dij, djk, dik]
                    tetraAngles = []
                    tetraAngles.extend(_triangle_stats(env, i, j, k))
                    tetraAngles.extend(_triangle_stats(env, center, j, k))
                    tetraAngles.extend(_triangle_stats(env, i, center, k))
                    tetraAngles.extend(_triangle_stats(env, i, j, center))
                    tetraDict[(i,j,k)] = [sorted(tetraDists), sorted(tetraAngles)]
                    


    if verbose:
        print('interatomic distances')
        print(distMatrix)
        print('angles through center')
        print(angleMatrix)
        print('tetrahedra')
        for tup, val in tetraDict.items():
            print(tup, ':', 'dists:', val[0], 'angles:',  val[1])
    return distMatrix, angleMatrix, tetraDict

def get_env_degen(env1, env2):
    """compares the distance, angle, and tetrahedral histograms to see if they *should* be degenerate"""
    d1, a1, tetra1 = get_env_stats(env1, verbose=False)
    d2, a2, tetra2 = get_env_stats(env2, verbose=False)
    distInv = np.all(np.sort(d1.flatten()) - np.sort(d2.flatten()) < 1e-8) #All distances the same within floating point errors
    angleInv = np.all(np.sort(a1.flatten()) - np.sort(a2.flatten()) < 1e-8)
    #Tetrahedra are the same if the sorted (but not labeled) list of [dists, angles] are equivalent
    tetraInv = True
    if len(tetra1) == len(tetra2): #
        tvals1 = sorted(list(tetra1.items()), key = lambda x: x[0])
        tvals2 = sorted(list(tetra2.items()), key = lambda x: x[0])
        for i in range(len(tvals1)):
            hasMatch = False
            for j in range(len(tvals2)):
                ti1, t1 = tvals1[i]
                ti2, t2 = tvals2[j]
                dInv = np.all(np.sort(np.array(t1[0])) - np.sort(np.array(t2[0])) < 1e-8) #All distances the same within floating point errors
                aInv = np.all(np.sort(np.array(t1[1])) - np.sort(np.array(t2[1])) < 1e-8)
                if dInv and aInv:
                    hasMatch = True
                    tvals2.pop(j)
                    break
            if not hasMatch:
                for j in range(len(tvals2)):
                    ti1, t1 = tvals1[i]
                    ti2, t2 = tvals2[j]
                    print(ti1, 'vs', ti2)
                    print('dists:', dInv)
                    print(t1[0])
                    print(t2[0])
                    print('angles:', aInv)
                    print(t1[1])
                    print(t2[1])

                tetraInv = False
    else:
        tetraInv = False
    print('Distances Same (2-Body): {}, Angles Same (3-body): {}, Tetrahedra Same (4-body): {}'.format(distInv, angleInv, tetraInv))


def gen_env_list(rc, n, atomsNum, seed=None, genType='rand', addRef=True, **kwargs):
    """Generate a list of environments, returns Ne (expected), Nb (upper). 
    n is the number of environments, rc is the critical radii, and atomsNum gives an equally likely list to choose the number of atoms to generate

    Heiarchy of seeding:
    'seed' -> Controls number of point selection and seeds default envseed and scatter generators. Each environment will be different but same between envLists
    'envseed' -> Controls point generation within each environments. Each environment will be the same but with different scatter.

    So if just setting 'seed' -> random environments and scatter but consistent between runs.
    If setting 'envseed' -> same environments but random scatter that's not consistent between runs.
    If setting both 'seed' and 'envseed' -> same environments with random scatter that's consistent between runs.
    If setting neither -> Nothing will be consistent between runs.

    """
    rng = np.random.RandomState(seed)
    if isinstance(atomsNum, (list, set, tuple)):
        atomsNum = list(atomsNum)
    else:
        atomsNum = [atomsNum]
    print('genType:', genType)
    envList = [] #list of points, seeds, and N (no atoms)
    envNs = []
    
        
    scatterseed = rng.randint(2**32-1)
    scatterRNG=np.random.RandomState(scatterseed)
    
    for i in range(n):
        N = atomsNum[rng.randint(0,len(atomsNum))]
        tkwargs = kwargs.copy()
        envseed = rng.randint(2**32-1) #Has to be called to keep consistency between runs.
        
        tkwargs['scatterRNG'] = scatterRNG
        if addRef and i == 0: 
            tkwargs['std'] = 0
        if 'envseed' in kwargs.keys(): 
            if not isinstance(kwargs['envseed'], type(None)): 
                envseed = kwargs['envseed']
        tkwargs['envseed']=envseed
        if genType in ['rand', 'random']:
            env = gen_random_env(rc, N=N, **tkwargs)
        elif genType == 'lattice':
            env = gen_lattice_env(rc, **tkwargs)
        elif genType == 'center':
            env = gen_center_env(rc, **tkwargs)
        elif genType in ['rot', 'rotate']:
            #Creates equal angle divisions
            env = gen_center_env(rc, azm= (i/n)*(2*np.pi), **tkwargs)
        elif genType == 'rotangle':
            env = gen_center_env(rc, azm= (i/n)*(2*np.pi), pol=(3/4)*np.pi, **tkwargs)
        elif genType == 'rotrandazm':
            env = gen_center_env(rc, azm= (i/n)*(2*np.pi), pol=rng.rand()*np.pi, **tkwargs)
        elif genType == 'line':
            env = gen_center_env(rc, r= ((i+1)/(n+1))*rc, **tkwargs)
        elif genType == 'translate':
            env = gen_center_env(rc, r=rc, **tkwargs)
            env = translate_env(env, translate=[-((i+1)/(n+1))*rc, 0, 0])
        elif genType == '2RefLineZ':
            env = gen_center_env(rc, r= ((i+1)/(n+1))*rc, **tkwargs)
            env.append(desd.cart2sph([0, 0, 1]))
        elif genType == '2RefLineX':
            env = gen_center_env(rc, r= ((i+1)/(n+1))*rc, **tkwargs)
            env.append(desd.cart2sph([-1, 0, 0]))
        elif genType == 'peturb':
            if addRef and i == 0: 
                tkwargs['std'] = 0
            env = gen_peturb_env(rc, **tkwargs)
        elif genType.startswith('counter'):
            if genType.endswith('Ref'):
                if addRef and i == 0: 
                    tkwargs['std'] = 0
                    tkwargs['degenType'] = not tkwargs['degenType'] 
            else:
                if i >= n/2 and 'degenType' not in tkwargs: tkwargs['degenType'] = True
                elif i < n/2 and 'degenType' not in tkwargs: tkwargs['degenType'] = False


            if genType.startswith('counterPower'):
                #print('power')
                env=gen_power_counter_env(rc, **tkwargs)
            elif genType.startswith('counterBispectrumRotate'):
                if i >= n/2: 
                    tkwargs['degenType'] = False 
                    env=gen_bispectrum_counter_env(rc, **tkwargs)
                    #rot= (i/n)*2*np.pi
                    #rotz= (i/n)*(2/3)*np.pi
                    rot= np.pi
                    rotz= (1/3)*np.pi
                    env = rotate_env(env, rotate = [0, rot, rotz],)
                elif i < n/2: 
                    tkwargs['degenType'] = False
                    env=gen_bispectrum_counter_env(rc, **tkwargs)
                    env = [env[0], env[1], env[7], env[6], env[5], env[4], env[3], env[2]]
            elif genType.startswith('counterBispectrum'):
                #print('bispectrum')
                env=gen_bispectrum_counter_env(rc, **tkwargs)

            elif genType.startswith('counterPlane'):
                #print('plane')
                env=gen_planar_counter_env(rc, **tkwargs)
            elif genType.startswith('counterMultiPlane'):
                #print('multiplane')
                if i <= n/3 and 'degenType' not in tkwargs: tkwargs['degenType'] = (True, True)
                elif n/3 <= i < 2*n/3 and 'degenType' not in tkwargs: tkwargs['degenType'] = (True, False)
                elif 2*n/3 <= i < n and 'degenType' not in tkwargs: tkwargs['degenType'] = (False, False)
                env=gen_multiplanar_counter_env(rc, **tkwargs)
            elif genType.startswith('counter'):
                #print('counter')
                env = gen_counter_env(rc, **tkwargs) 
            else: raise ValueError('gen_env_list: Unknown genType "{}"'.format(genType))
        else:
            raise ValueError('gen_env_list: Unknown genType "{}"'.format(genType))
        
        if 'rotate' in kwargs:
            env = rotate_env(env, rotate = kwargs['rotate'],)
        if 'translate' in kwargs:
            env = translate_env(env, translate = kwargs['translate'], )
        if 'invert' in kwargs:
            env = invert_env(env, invert = kwargs['invert'],)
        if 'rotateFull' in kwargs:
            rot= (i/n)*2*np.pi
            env = rotate_env(env, rotate = [0, rot,0],)
        

        envN = len(env)
        envList.append([env, envseed, envN])
        envNs.append(envN)
    
    Ne = np.mean(envNs)
    Nb = np.max(envNs)
    return envList, Ne, Nb 

def gen_descriptors(rc, envList, q, Ne, Nb, explicit=True, maxp=np.inf, maxparam=np.inf, useChiVersion=False, verbose=False, multiprocessing = False, cleanMP=True, cores=1, doPlot=False):
    EijListFull =[]
    desFull =[]
    desList = [None for i in range(len(envList))]
    if multiprocessing:
        if mpu.mpOn == False:
            mpu.startMP(cores=cores)
        for i in range(len(envList)):
            envTup=envList[i]
            env, locseed, envN = envTup
            if useChiVersion:
                mpu.pushJobMP((i, desd.get_chi_descriptor, [rc, env, q, Nb, Ne], {'maxp':maxp, 'maxparam':maxparam, 'verbose':False, 'explicit':explicit}), verbose=False)
            else:
                mpu.pushJobMP((i, desd.get_descriptor, [rc, env, q, Nb, Ne], {'maxp':maxp, 'maxparam':maxparam, 'verbose':False, 'explicit':explicit}), verbose=False)
        mpu.waitResMP(len(envList))
        resList = mpu.popResQueue()
        if cleanMP:
            mpu.endMP()
        for i, resTup in resList:
            #print(i, resTup)
            des, EijList = resTup
            EijListFull.extend(EijList)
            desFull.extend(des)
            desList[i]=des
            
    else:
        for i in range(len(envList)):
            print(i,'/', (len(envList)-1))
            envTup=envList[i]
            env, locseed, envN = envTup
            if useChiVersion:
                des, EijList= desd.get_chi_descriptor(rc, env, q, Nb, Ne, maxp=maxp, maxparam=maxparam, verbose=True, explicit=explicit)
            else:
                des, EijList = desd.get_descriptor(rc, env, q, Nb, Ne, maxp=maxp, maxparam=maxparam, verbose=True, explicit=explicit)
            EijListFull.extend(EijList)
            desFull.extend(des)
            desList[i] = des
            #print(EijList, des) 
    
    if doPlot:
        fig, axs = plt.subplots(1,2)
        axs[0].hist(EijListFull, bins = 20)
        axs[0].set_xlabel('Eij values')
        axs[1].hist(desFull, bins = 20)
        axs[1].set_xlabel('descriptor values')

    return desList

def compare_env(rc, envList, desList, distType = 'L1', compareMetric = 'GW', forceSym = True, doPlot=False, ref=None, verbose=False, compareDesArgs = [], compareDesKwargs = {}):
    scoreDict = dict() #(i,j):{pairwise descriptor, pairwise crgw}
    
            
    print(ref) 
    if isinstance(ref, type(None)): istart = 0; iend = len(envList)
    else: istart = ref; iend = ref+1

    if compareMetric in ['G2', 'G3', 'SOAP', 'bispectrum']:
        import ase
        import dscribe.descriptors as dd
        if compareMetric in ['bispectrum']:
            import pyxtal_ff.utilities as pyffutil 

        
        atomEnvList = []
        params = []
        atomTypes = []
        compareDes = []
        for i in range(0, len(envList)):
            envTup1 = envList[i]
            env1, locseed1, envN1 = envTup1
            pos = []
            for p in env1:
                pos.append(desd.sph2cart(p))
            #Set parameters.
            if compareMetric == 'G2': params.append((1,0)) #n, Rs, come back to this later.
            elif compareMetric == 'G3': params.append((1))
            elif compareMetric in ['G4', 'G5']: params.append((1,1,1))
            #convert enviornment to ASE environment.
            atomTypes.append('H')
            aseEnv = ase.Atoms('H{}'.format(len(env1)), positions = pos)
            aseEnv.set_cell(rc*np.identity(3))
            atomEnvList.append(aseEnv)
            #get_env_stats(env1)

            if compareMetric == 'bispectrum':
                compareDes.append(pyffutil.compute_descriptor({'type': 'SO4', 'parameters':{'lmax':3, 'normalize_U':False}, 'cutoff':'exp', 'Rc': rc}, aseEnv))

        if compareMetric.startswith('G'):
            ACSF = dd.ACSF(rc, species=atomTypes, **{'g{}_params'.format(compareMetric[1]):params})
            compareDes = ACSF.create(atomEnvList)
        elif compareMetric == 'SOAP':
            SOAP = dd.SOAP(rc, nmax=9, lmax=9, species=atomTypes)
            compareDes = SOAP.create(atomEnvList)
        #print(compareDes.shape)
        #print(compareDes)
    elif compareMetric.startswith('chi'):
        if compareMetric in ['chi']:
            compareDesKwargs['useChiVersion'] = True
            compareDes = gen_descriptors(*compareDesArgs, **compareDesKwargs)
        elif compareMetric.startswith('chi_') or compareMetric in ['chiReplace']:
            print('WARNING: Replacing the descriptors with their Chi variant. Possibly use -d 0 with this')
            if compareMetric.startswith('chi_'): 
                compareMetric =  compareMetric.strip('chi_')
            elif compareMetric in ['chiReplace']:
                compareMetric =  'GW' 

            compareDesKwargs['useChiVersion'] = True
            desList = gen_descriptors(*compareDesArgs, **compareDesKwargs)
       

    for i in range(istart, iend):
        if forceSym and isinstance(ref, type(None)): jstart = i
        else: jstart = 0
        for j in range(jstart, len(envList)):
            envTup1 = envList[i]
            envTup2 = envList[j]
            env1, locseed1, envN1 = envTup1
            env2, locseed2, envN2 = envTup2
            
            #get_env_degen(env1, env2)
            
            des1=desList[i]
            des2=desList[j]
            #if distType == 'L1': desDist = np.sum(np.abs(des1-des2)) 
            #elif distType == 'L2': desDist = np.sqrt(np.sum((des1-des2)**2))
            if distType == 'L1': distFunc = distL1cart
            elif distType == 'L2': distFunc = distL2cart
            elif distType.lower() in ['zeros', 'zero', '0']: distFunc = distL2cart
            else:
                raise ValueError('compare_env(): Unknown distType. Must be "L1" or "L2". Got {}'.format(distType))
            desDist = distFunc(des1, des2)
            compDist = 0
            if compareMetric == 'GW':
                if verbose:
                    print('1', env1)
                    print('2', env2)
                compDist, G = crgw.get_ae_dist(rc, env1, rc, env2)
                #compDist2, G = crgw.get_ae_dist(rc, env1, rc, env2)
                #compDist = min(compDist1, compDist2)
            elif compareMetric == 'L1': #sum of the displacements
                for k in range(len(env1)): #cannot compare environments of different atoms and atoms must have the same ordering
                    p1 = env1[k]
                    p2 = env2[k]
                    compDist += distL1(p1, p2)
            elif compareMetric == 'L2':
                for k in range(len(env1)):
                    p1 = env1[k]
                    p2 = env2[k]
                    compDist += distL2(p1, p2)
            elif compareMetric in ['G2', 'G3', 'SOAP']:
                #print('i', compareDes[i, :, :].flatten())
                #print('j', compareDes[j, :, :].flatten())
                compDist = distFunc(compareDes[i, :, :], compareDes[j, :, :])
            elif compareMetric in ['bispectrum']:
                cd1= compareDes[i]['x'] #Bispectrum environments have 'x', 'dxdr' and 'rdxdr'
                cd2=compareDes[j]['x']
                compDist = distFunc(cd1, cd2)
            elif compareMetric in ['chi']:
                compDist = distFunc(compareDes[i], compareDes[j])
            if verbose:
                print((i,j),'des:', desDist, 'comp:', compDist)
            #print('env1:', env1)
            #print('env2:', env2)
            scoreDict[(i,j)]=(desDist, compDist)
    if doPlot:
        fig, ax = plt.subplots(1,1)
        scoreArray = np.array(scoreDict.values())
        ax.scatter(scoreArray[:,1], scoreArray[:,0])
        ax.set_xlabel('GW Dist')
        ax.set_ylabel('Descriptor')
        #plt.show()
    return scoreDict

def minq(Nb):
    #Double check that this calculation is correct with the number of parameters generated
    C = lambda n, r: spf.factorial(n)/(spf.factorial(r)*spf.factorial(n-r))
    nqList = lambda q, Nb: [C(q-2*r+2, 2)*C(r+2, 2) for r in range(0, int(np.floor(q/2)))]
    nq = lambda q, Nb: Nb**2*np.sum([C(q-2*r+2, 2)*C(r+2, 2) for r in range(0, int(np.floor(q/2)))])


    q = -1
    qsum = 0
    #while qsum < 3*(Nb-1):
    while qsum < 3*Nb**2*(Nb-1): 
        q+=1
        val = nq(q, Nb)
        #qsum += nq(q, Nb)
        trueparam = len(desd.get_parameters(q, Nb, maxp = Nb**2))
        qsum = trueparam
        print('q:', q, 'nq:', val, 'sum(nq):', qsum, '>=?',3*(Nb-1), 'or', 3*Nb**2*(Nb-1), 'generated:',  trueparam)
        #print(Nb, nqList(q, Nb))
    return q

def minq_param(Nb, maxp=None, maxparam=None):
    #Double check that this calculation is correct with the number of parameters generated

    q = -1
    qsum = 0
    while qsum < maxparam:
        q+=1
        if isinstance(maxp, type(None)):
            maxp = Nb**2
        trueparam = len(desd.get_parameters(q, Nb, maxp = maxp))
        qsum = trueparam
        print('q:', q, 'generated:', qsum, '>=', 3*Nb**2*(Nb-1), '(min) >= ', maxparam)
        #print(Nb, nqList(q, Nb))
    return q

def check_chi_var_sum(rc, n=50, atomsNum =3, q=1, maxp=1, seed=None, **kwargs):
    #For a fixed i, j atoms checks that the variance of chi all other atoms k1-k10 in the same environment is equal to the variance of
    #all kth atoms in all environments
    print('Number of Environments:', n)
    print('Atoms Per Environment:', atomsNum)
    envListij, Ne, Nb = gen_env_list(rc, 1, atomsNum = 2, seed=seed)
    envList, Ne, Nb = gen_env_list(rc, n, atomsNum = atomsNum, seed=seed) #HEADS UP, THESE USE THE SAME SEED
    paramList = desd.get_parameters(q, Nb, maxp=maxp)
    ri = envListij[0][0][0]
    rj = envListij[0][0][1]
    chiArray = np.zeros((n, atomsNum)) #every k atom except the first two by n environments
    
    n0,n1,n2,l0,l1,l2,p = paramList[-1][1] #get different parameters based on what's last
    for ni in range(n):
        #print(ni)
        for ki in range(atomsNum):
           rk = envList[ni][0][ki]
           #print('ri', ri)
           #print('rj', rj)
           #print('rk', rk)
           
           chiArray[ni, ki] = desd.get_chi2(rc, n0, n1, n2, l0, l1, l2, ri, rj, rk)

    #Var(Sum(env)) Variance of the sums
    chiSumEnv = np.sum(chiArray, axis=1)
    chiSumEnvVar = np.var(chiSumEnv)
    chiVarRk = np.var(chiArray, axis=0)
    chiVarRkSum = np.sum(chiVarRk)
    print('rnk, n is the environment number, k is the kth atom in environment')
    #print('Var of Env. Sums [var(r11+r12+r13..., r21+r22+r23...)]: {}'.format(chiSumEnvVar))
    #print('Var of Rkth Sums [var(r11+r21+r31..., r12+r22+r32)]: {}'.format(chiSumRkVar))
    print('Var of Sums var(r11+r12+r13, r21+r22+r23): {}'.format(chiSumEnvVar))
    print('Sum of Vars var(r11, r21, r31) + var(r12,r22,r32): {}'.format(chiVarRkSum))

def calc_Eij_var(rc, m0, m0p, n=50, atomsNum = 3, q=1, maxp=1, seed=None, **kwargs):
    print('Number of Environments:', n)
    print('Atoms Per Environment:', atomsNum)
    envListij, Ne, Nb = gen_env_list(rc, 1, atomsNum = 2, seed=seed)
    envList, Ne, Nb = gen_env_list(rc, n, atomsNum = atomsNum, seed=seed) #HEADS UP, THESE USE THE SAME SEED
    paramList1 = desd.get_parameters(q, Nb, maxp=maxp)
    paramList2 = desd.get_parameters(q, Nb, maxp=maxp)
    ri = envListij[0][0][0]
    rj = envListij[0][0][1]
    config1, param1 = paramList1[-1]
    config2, param2 = paramList2[0]
    n0,n1,n2,l0,l1,l2,p = param1
    n0p,n1p,n2p,l0p,l1p,l2p,pp = param2
    
    varsum = 0
    for ki in range(atomsNum):
        print(ki, atomsNum)
        for m0 in range(-l0,l0+1):
            for m0p in range(-l0p,l0p+1):
                C,m1,m2 = desd.clebsch_gordan(l1,l2,l0,m0)
                Cp,m1p,m2p = desd.clebsch_gordan(l1,l2,l0,m0p)
                for a in range(0, np.size(C)):
                    for b in range(0, np.size(Cp)):
                        zstarList = []
                        zpList = []
                        
                        for ni in range(n):
                            rk = envList[ni][0][ki]
                            zstar = np.conjugate(desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0,m1[a],m2[a]))
                            zp = desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0p,m1p[b],m2p[b])
                            zstarList.append(zstar)
                            zpList.append(zp)
                        zstarList=np.array(zstarList)
                        zpList = np.array(zpList)

                        Ezstar = np.mean(zstarList)
                        Ezp = np.mean(zpList)
                        Eboth = np.mean(zstarList * zpList)
                        
                        varsum += C[a]*Cp[b]*(Eboth-(Ezstar*Ezp))
    print(varsum*(1/(2*l0+1)))



def check_z_mean(rc, m0, m0p, n=50, atomsNum = 3, q=1, maxp=1, seed=None, allParams=False, ls0Only=True, **kwargs):
    print('Number of Environments:', n)
    print('Atoms Per Environment:', atomsNum)
    envListij, Ne, Nb = gen_env_list(rc, 1, atomsNum = 2, seed=seed)
    envList, Ne, Nb = gen_env_list(rc, n, atomsNum = atomsNum, seed=seed) #HEADS UP, THESE USE THE SAME SEED
    paramList1 = desd.get_parameters(q, Nb, maxp=maxp)
    paramList2 = desd.get_parameters(q, Nb, maxp=maxp)
    ri = envListij[0][0][0]
    rj = envListij[0][0][1]
    config1, param1 = paramList1[-1]
    config2, param2 = paramList2[0]
    
    
    zstarList = []
    zpList = []
    if allParams:
        for i in range(len(paramList1)):
            config1, param1 = paramList1[i]
            print('{}/{}'.format(i, len(paramList1)-1))
            n0,n1,n2,l0,l1,l2,p = param1
            print('(n0,n1,n2,l0,l1,l2,p)\n{}'.format( param1))
            C,m1l,m2l = desd.clebsch_gordan(l1,l2,l0,m0)
            #C,m1pl,m2pl = desd.clebsch_gordan(l1p,l2p,l0p,m0p)
            C,m1pl,m2pl = desd.clebsch_gordan(l1,l2,l0,m0p)
            for ci in range(len(C)):
                m1 = m1l[ci]
                m2 = m2l[ci]
                m1p = m1pl[ci]
                m2p = m2pl[ci]
            
                print("(m0, m1, m2)=({},{},{}), (m0', m1', m2')=({},{},{})".format(int(m0), int(m1), int(m2), int(m0p), int(m1p), int(m2p)))
                for ni in range(n):
                    for ki in range(atomsNum):
                        rk = envList[ni][0][ki]
                        zstar = np.conjugate(desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0,m1,m2))
                        zp = desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0p,m1p,m2p)
                        zstarList.append(zstar)
                        zpList.append(zp)
    else:
        n0,n1,n2,l0,l1,l2,p = param1
        n0p,n1p,n2p,l0p,l1p,l2p,pp = param2
        print('(n0,n1,n2,l0,l1,l2,p)', param1)
        print('(n0,n1,n2,l0,l1,l2,p)\n{}\n{}'.format( param1, param2))
        C,m1,m2 = desd.clebsch_gordan(l1,l2,l0,m0)
        C,m1p,m2p = desd.clebsch_gordan(l1,l2,l0,m0p)
        m1 = m1[-1]
        m2 = m2[-1]
        m1p = m1p[-1]
        m2p = m2p[-1]
        print("m0:{}, m1:{}, m2:{}, m0':{}, m1':{}, m2':{}, ".format(m0, m1, m2, m0p, m1p, m2p))
        for ni in range(n):
            for ki in range(atomsNum):
                rk = envList[ni][0][ki]
                zstar = np.conjugate(desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0,m1,m2))
                zp = desd.get_z(rc,n0,n1,n2,l0,l1,l2,ri,rj,rk,m0p,m1p,m2p)
                zstarList.append(zstar)
                zpList.append(zp)

    zstarList=np.array(zstarList)
    zpList = np.array(zpList)

    Ezstar = np.mean(zstarList)
    Ezp = np.mean(zpList)
    Eboth = np.mean(zstarList * zpList)
    print('Number of Zs: {}'.format(len(zstarList)))
    print("E(z'z*):{},\nE(z*):{},\nE(z'):{},\nE(z*)E(z'):{}".format(Eboth, Ezstar, Ezp, Ezstar*Ezp))
    print('(4*pi*rc^3 /3)^-3): {}'.format(((4*np.pi*rc**3)/3)**(-3)))

def check_psi0(rc, params1, params2, n=50, atomsNum = 1, q=1, maxp=1, seed=None, allParams=False, ls0Only=True, plot=True, **kwargs):
    #params1 and params2 are lists of [(n,l,m)]
    print('Number of Atoms:', n)
    #print('(n0,n1,n2,l0,l1,l2,p)', param1)
    
    psiDict = dict()
   
    #for nc,lc,mc in params1: #z*
    #    for npc,lp,mp in params2: #z'
    for i in range(len(params1)):
            nc,lc,mc in params1[i] #z*
            npc,lp,mp in params2[i] #z'
            print("Psi*({},{},{}), Psi'({},{},{})".format(nc,lc,mc,npc,lp,mp))
            psiMeanList = []
            psiMean2List = []
            for ni in n: 
                psiList = []
                psi2List = []
                envList, Ne, Nb = gen_env_list(rc, int(ni), atomsNum = atomsNum, seed=seed) #HEADS UP, THESE USE THE SAME SEED
                for nk in range(len(envList)):
                    rk = envList[nk][0][0]
                    psi = desd.get_Psinlm(rc, nc, lc, mc, rk)
                    psi2 = desd.get_Psinlm(rc, npc, lp, mp, rk)
                    psiList.append(psi)
                    psi2List.append(np.conjugate(psi)*psi2)
                #print(psi)
                psiMeanList.append(np.mean(psiList))
                psiMean2List.append(np.mean(psi2List))
                
            psiDict[(nc,lc,mc,npc,lp,mp)]=[np.array(psiMeanList), np.array(psiMean2List)]



    if plot:
        fig, ax = plt.subplots(1,1)
        for tup, data in psiDict.items():
            nc,lc,mc, npc,lp,mp= tup
            psiMean, psi2Mean = data
            print("E[Psi*({},{},{})]={}, E[Psi'({},{},{})]={}".format(nc,lc,mc, psiMean,npc,lp,mp,psi2Mean))

            if plot:
                ax.semilogx(n, psiMean, label='E[Psi({},{},{})]'.format(nc,lc,mc))
                ax.semilogx(n, psi2Mean, linestyle='--', label='E[Psi({},{},{}) Psi({},{},{})]'.format(nc,lc,mc,npc,lp,mp))
                        
        ax.set_xlabel('no. atomic environmens')
        ax.legend()
        ax.set_ylim(-0.1,0.1)
        plt.show()



def check_Eij_var(rc, n=50, atomsNum = 3, q=1, maxp=1, seed=None, **kwargs):
    EijList = [] #All ENj
    perEnvVarList = [] #Average Variance of n environments w/ all parameters
    perParamVarList = [] #Average Variance for a single environment for a single parameter tuple
    envList, Ne, Nb = gen_env_list(rc, n, atomsNum = atomsNum, seed=seed)
    paramList = desd.get_parameters(q, Nb, maxp=maxp)
    k=1
    for env, envSeed, N in envList:
        print('{}/{} | {}, N:{}, no. param: {}'.format(k, len(envList), envSeed, N, len(paramList)))
        k+=1
        EnvVarList = []
        for param in paramList:
            #print(envSeed,param)
            n0,n1,n2,l0,l1,l2,p=param[1]
            #print('n0:{}, n1:{}, n2:{}'.format(n0,n1,n2))
            #print('l0:{}, l1:{}, l2:{}'.format(l0,l1,l2))
        
            #N = len(env)
            ConfigVarList = []
            for i in range(len(env)):
                for j in [j for j in range(len(env)) if i != j]:
                    Eij = desd.get_Eij(i, j,rc, env, n0,n1,n2,l0,l1,l2)
                    EijList.append(Eij)
                    ConfigVarList.append(Eij)
                    EnvVarList.append(Eij)
                    #print(envSeed, i, j, Eij)
            perParamVarList.append(np.var(ConfigVarList))
        perEnvVarList.append(np.var(EnvVarList))
    
    EijList = np.array(EijList)
    EijVar = np.var(EijList)

    perEnvAvgVar=np.mean(perEnvVarList)
    perParamAvgVar=np.mean(perParamVarList)
    EijVarTheo = (((4*np.pi*rc**3)**(-3))/3)*atomsNum
    
    print('Eij Var: {}, n={}\nAvg Env Eij Var: {}\nAvg Param Eij Var: {}\n Expected ((4*pi*rc^3)^-3) / 3 )*N = {}'.format(EijVar, len(EijList), perEnvAvgVar, perParamAvgVar, EijVarTheo))
        
    return EijVar,perEnvAvgVar, perParamAvgVar, EijVarTheo, EijList

#Chi tests:
#Note, the original matlab implimentation has a direct get_chi and an explicit get_chi2. Get chi will return complex numbers
#for l0+l1+l2 is odd where as get_chi2 will return effectively zero.
#Chi should be SO(3) and O(3) invariant

def check_chi_SO3(N=1, rc=5, seed=None, tol=1000*eps, transformFunc = lambda x: x, chiFunc = desd.get_chi2, **kwargs):
    """Where N is the number of points checked.
    rc is the critical radii
    seed is used for reproducability
    tol is the amount of difference allowed between chi values
    
    kwargs:
    x0,x1,x2 are a numpy array of [x,y,z]
    r0,r1,r2 are a numpy array of [r, pol, azm]
    
    n0,n1,n2 are integers
    l0,l1,l2 are integers

    M is a 3x3 matrix used to get the rotation matricies U and V by SVD 
    """
    #The group of all rotations about the origin
    rng = np.random.RandomState(seed)
    rng2 = np.random.RandomState(seed)
    #critical radii
    #generate 3 random points
    if 'x0' in kwargs: x0 = kwargs['x0']
    else: x0=np.sqrt(3)*rc*(rng.random(3)-1/2)*2
    if 'x1' in kwargs: x1 = kwargs['x1']
    else: x1=np.sqrt(3)*rc*(rng.random(3)-1/2)*2
    if 'x2' in kwargs: x2 = kwargs['x2']
    else: x2=np.sqrt(3)*rc*(rng.random(3)-1/2)*2
    print('Points (cart):\n{}\n{}\n{}'.format(x0, x1, x2))
    cartpoints = np.zeros((3,3))
    cartpoints[ 0,:] = x0
    cartpoints[ 1,:] = x1
    cartpoints[ 2,:] = x2


    #set parameters
    if 'n0' in kwargs: n0 = kwargs['n0']
    else: n0 = rng.randint(0, 10)
    if 'n1' in kwargs: n1 = kwargs['n1']
    else: n1 = rng.randint(0, 10)
    if 'n2' in kwargs: n2 = kwargs['n2']
    else: n2 = rng.randint(0, 10)

    #l0+l1+l2 must be even. Otherwise chi will be zero.
    if 'l0' in kwargs: l0 = kwargs['l0']
    else: l0 = rng.randint(0, 10)
    if 'l1' in kwargs: l1 = kwargs['l1']
    else: l1 = None 
    if 'l2' in kwargs: l2 = kwargs['l2']
    else: l2 = None 

    #l0 < l1 + l2 and l0 > np.abs(l1 - l2). Allows user to set parameters
    if isinstance(l1, type(None)) or isinstance(l2, type(None)):
        l1 = rng2.randint(0, 10)
        l2 = rng2.randint(0, 10)
        while l0 > l1 + l2 or l0 < np.abs(l1-l2):
            l1 = rng2.randint(0, 10)
            l2 = rng2.randint(0, 10)
    print('n0:{}, n1:{}, n2:{}'.format(n0,n1,n2))
    print('l0:{}, l1:{}, l2:{}'.format(l0,l1,l2))

    pointPairs = np.zeros((N,2))
    for ni in range(N):
        #Random transformation
        if 'M' in kwargs: M = kwargs['M']
        else: M = rng.random((3,3))
        print('M=\n',M)
        U, temp, V = sp.linalg.svd(M, lapack_driver='gesdd')
        V = V.transpose()
        if np.linalg.det(U) < 0: U = -U
        if np.linalg.det(V) < 0: V = -V

        print('U:', U)
        print('V:', V)


        cart = np.matmul(U,cartpoints.transpose()).transpose()
        if 'r0' in kwargs: r0 = kwargs['r0']
        else: r0 = desd.cart2sph(cart[0,:])
        if 'r1' in kwargs: r1 = kwargs['r1']
        else: r1 = desd.cart2sph(cart[1,:])
        if 'r2' in kwargs: r2 = kwargs['r2']
        else: r2 = desd.cart2sph(cart[2,:])
        print(cart) 
        print('A (sph):\nr0={}\nr1={}\nr2={}'.format(r0, r1, r2))
        A = chiFunc(rc, n0, n1, n2, l0, l1, l2, r0, r1, r2)

        #cartpoints = transformFunc(cartpoints) 
        cart = np.matmul(V,cartpoints.transpose()).transpose()
        r0 = desd.cart2sph(cart[0,:])
        r1 = desd.cart2sph(cart[1,:])
        r2 = desd.cart2sph(cart[2,:])
        if 'r0' in kwargs: r0 = kwargs['r0']
        else: r0 = desd.cart2sph(cart[0,:])
        if 'r1' in kwargs: r1 = kwargs['r1']
        else: r1 = desd.cart2sph(cart[1,:])
        if 'r2' in kwargs: r2 = kwargs['r2']
        else: r2 = desd.cart2sph(cart[2,:])
        print(cart) 
        print('B (sph):\nr0={}\nr1={}\nr2={}'.format(r0, r1, r2))
        B = chiFunc(rc, n0, n1, n2, l0, l1, l2, r0, r1, r2)
        print('Chi:', A, B)
        pointPairs[ni, :] = np.array([A,B])
    print(pointPairs)
    print('n0:{}, n1:{}, n2:{}'.format(n0,n1,n2))
    print('l0:{}, l1:{}, l2:{}'.format(l0,l1,l2))
    if np.any(np.abs(pointPairs - (np.zeros((N,2))+pointPairs[0]))>tol):
        print('Chi difference exceeds tolerance. Not SO3 invariant')
    else:
        print('Chi difference does not exceed tolerance.')
