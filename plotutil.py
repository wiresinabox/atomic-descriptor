import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as pltc
import matplotlib.collections as pltcol
import matplotlib.colors as pltcolors
import os

def _make_dict_keys_str(inDict):
    returnDict = {}
    #Recursively makes keys strings for json
    #print('in', inDict)
    for key, val in inDict.items():
        if isinstance(val, dict): val = _make_dict_keys_str(val)
        elif isinstance(val, list): 
            for i in range(len(val)):
                if isinstance(val[i], dict): val[i]=_make_dict_keys_str(val[i])
                if isinstance(val[i], np.ndarray): val[i] = val[i].tolist()
        else: key = str(key)
        if isinstance(val, np.ndarray):
            val = val.tolist()
        if isinstance(val, (np.int, np.int0, np.int8, np.int16, np.int32, np.int64, )): val = int(val)
        if isinstance(val, (np.float,  np.float16, np.float32, np.float64, np.float128)): val = float(val)
        returnDict[str(key)] = val
    #print('out',returnDict)
    return returnDict

def _unmake_str(val):
    convs = [int, float]
    for conv in convs:
        try:
            val = conv(val)
            break
        except ValueError:
            continue
    return val
    

def _unmake_dict_keys_str(inDict):
    #Do the opposite
    returnDict = {}
    for key, val in inDict.keys():
        if isinstance(val, dict): _make_dict_keys_str(val)
        else: 
            if key.startswith('('): #Tuple
                key = (_unmake_str(v) for v in key[1,:-1].split(','))

            returnDict[key] = val
    return returnDict


def saveRunJson(outfn,scoreDicts, infoDict={}, errDict = {}):
    if isinstance(scoreDicts, list):
        scoreDicts = {i:scoreDicts[i] for i in range(len(scoreDicts))}
    
    saveScoreDicts = _make_dict_keys_str(scoreDicts)
    saveErrDict = _make_dict_keys_str(errDict)
    for key, val in infoDict.items():
        if isinstance(val, (np.int, np.int0, np.int8, np.int16, np.int32, np.int64, )): infoDict[key] = int(val)
        if isinstance(val, (np.float,  np.float16, np.float32, np.float64, np.float128)): infoDict[key] = float(val)




    saveDict = {
            'info':infoDict,
            'scores':saveScoreDicts,
            'errors':saveErrDict,
            }
    #print(saveDict)
    outFile = open(outfn, 'w')
    json.dump(saveDict, outFile, indent = 1)

def loadRunJson(jsonfn):
    jsonFile = open(jsonfn, 'r')
    saveDict = json.load(jsonFile)
    return saveDict['info'], saveDict['scores'], saveDict['errors']

def smokePlot(scoreDicts, infoDict = {}, fig = None, ax=None, normalize=False, sqrtNorm=False, reverseAxis=False, labels=None, plotKwargs = None, returnHandles=False, colormap = None, reverse=False, refs=None, colorbar=False, slopeDiv = 0, **kwargs):
    if isinstance(scoreDicts, str):
        infoDict, scoreDicts, errDict= loadRunJson(scoreDicts)

    if isinstance(fig, type(None)) and isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    elif isinstance(ax, type(None)):
        ax = fig.add_subplot()
    elif isinstance(fig, type(None)):
        fig = ax.get_figure()
    print(fig, ax)
    handles = []
    
    if colorbar: 
        upperParam = -np.inf
        lowerParam = np.inf
        for i, scoreDict in enumerate(scoreDicts.values()): 
            nparams= scoreDict['nparams']
            upperParam = max(upperParam, nparams)
            lowerParam = min(lowerParam, nparams)
        print(upperParam, lowerParam)
        normP = pltcolors.Normalize(vmin=lowerParam, vmax=upperParam)
        colormapScalarP = pltc.ScalarMappable(norm=normP, cmap=colormap)
        #cmap = colormapScalarP.get_cmap()
        cmap = pltc.get_cmap(colormap)
    xlim = np.array([np.inf, -np.inf])
    ylim = np.array([np.inf, -np.inf])
    nparams = np.array([val['nparams'] for key, val in sorted(list(scoreDicts.items()), reverse=reverse,key = lambda x: int(x[0]))])
    slopeDivList = []
    if slopeDiv >= 1: slopeDivList.append(np.min(nparams))
    if slopeDiv >= 2: slopeDivList.append(np.max(nparams))
    if slopeDiv >= 3: #Do roughly equally spaced intervals.
        div = int((np.max(nparams) - np.min(nparams))/(slopeDiv-1))
        for i in range(1, slopeDiv-1):
            print(i, (div*i + np.min(nparams)))
            slopeDivList.append(nparams[np.argmin(np.abs(nparams - (div*(i) + np.min(nparams)) ))])

    print(slopeDiv, slopeDivList)
    valsList = []
    for i, scoreDict in enumerate([val for key, val in sorted(list(scoreDicts.items()), reverse=reverse,key = lambda x: int(x[0]))]): 
        scores= scoreDict['scores']  
        nparams= scoreDict['nparams']  
        Ne= scoreDict['Ne'] 
        
        vals = []
        for key, tup in scores.items():
            if isinstance(key, str):
                key = key.strip('()').split(',')
            if isinstance(refs, (list, tuple, np.ndarray)):
                ind1, ind2 = key 
                ind1 = int(ind1)
                ind2 = int(ind2)
                if ind1 not in refs: continue

            dist, comp = tup 
            if reverseAxis:
                vals.append([dist,comp])
            else:
                vals.append([comp, dist])
        vals = np.array(vals)
        if normalize and sqrtNorm: norm = np.sqrt(nparams)
        elif normalize and not sqrtNorm: norm = nparams
        else: norm = 1
        valsList.append(vals)

        xlim[0] = min(xlim[0], np.min(vals[:, 0]))
        xlim[1] = max(xlim[1], np.max(vals[:, 0]))
        ylim[0] = min(ylim[0], np.min(vals[:, 1]/norm))
        ylim[1] = max(ylim[1], np.max(vals[:, 1]/norm))
        
        plotk = kwargs
        
        if not isinstance(labels, type(None)): plotk['label'] = labels[i]
        elif 'label' in scoreDict: plotk['label']= scoreDict['label']  
        else: plotk['label'] = 'Ne: {}, nparams: {}'.format(Ne, nparams)
        if 'color' in scoreDict: plotk['color']= scoreDict['color']
        elif colorbar: plotk['color'] = cmap(normP(nparams))

        if isinstance(plotKwargs, dict): plotk.update(plotKwargs)
        elif isinstance(plotKwargs, list): plotk.update(plotKwargs[i])
        A = ax.scatter(vals[:,0], vals[:,1]/norm, **plotk) 
        handles.append(A)
        
        #Upper and lower slopes
        #Get slopes for each point and the origin, takes the min and max
    xlim = xlim *1.1
    ylim = ylim *1.1
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    print(infoDict.keys())
    if 'distType' in infoDict:
        if reverseAxis: ax.set_xlabel(infoDict['distType'])
        else: ax.set_ylabel(infoDict['distType'])
    
    if 'compareMetric' in infoDict:
        if reverseAxis: ax.set_ylabel(infoDict['compareMetric'])
        else: ax.set_xlabel(infoDict['compareMetric'])

    if colorbar:
        fig.colorbar(colormapScalarP, ax = ax)

    #Lines are plotted seperately b/c of axis movement.
    for i, scoreDict in enumerate([val for key, val in sorted(list(scoreDicts.items()), reverse=reverse,key = lambda x: int(x[0]))]):
        vals = valsList[i]
        scores= scoreDict['scores']  
        nparams= scoreDict['nparams']  
        Ne= scoreDict['Ne'] 
        if not isinstance(labels, type(None)): plotk['label'] = labels[i]
        elif 'label' in scoreDict: plotk['label']= scoreDict['label']  
        else: plotk['label'] = 'Ne: {}, nparams: {}'.format(Ne, nparams)
        if 'color' in scoreDict: plotk['color']= scoreDict['color']
        elif colorbar: plotk['color'] = cmap(normP(nparams))
        if normalize and sqrtNorm: norm = np.sqrt(nparams)
        elif normalize and not sqrtNorm: norm = nparams
        else: norm = 1
        
        if nparams in slopeDivList:
            vals = vals[vals[:,0] != 0, :]
            vals[:,1] = vals[:,1]/norm #normalize the whole dang thing so you don't have to deal with it later
            slopex = 1000
            slopes = vals[:,1]/vals[:,0]
            
            maxSlope = np.max(slopes)
            maxPoint = vals[np.argmax(slopes), :]
            maxRot = np.degrees(np.arctan2(maxSlope*slopex, slopex))
            maxRotTrans = ax.transData.transform_angles(np.array([maxRot]), maxPoint.reshape((1,2)))[0]
            minSlope = np.min(slopes)
            minPoint = vals[np.argmin(slopes), :]
            minRot = np.degrees(np.arctan2(minSlope*slopex, slopex))
            minRotTrans = ax.transData.transform_angles(np.array([minRot]), minPoint.reshape((1,2)))[0]
            

            slopekmax = {'linestyle':'-', 'color':plotk['color'], 'zorder':4, 'linewidth':2,}
            slopekmin = {'linestyle':'-', 'color':plotk['color'], 'zorder':4, 'linewidth':2,}
            textkmax = {
                'zorder':4,
                'rotation_mode':'anchor',
                'ha':'right',
                'fontfamily':'monospace',
                }
            textkmin= {
                'zorder':4,
                'rotation_mode':'anchor', 
                'ha':'right',
                'fontfamily':'monospace',
                }

            slopekout = slopekmax.copy()
            slopekout.update({'color':'white', 'zorder':3, 'linestyle':'-', 'linewidth':4})
            textkout = textkmax.copy()
            textkout.update({'color':'white', 'zorder':3, 'fontweight':1000})
            
            if xlim[1]*maxSlope > ylim[1]:
                maxTextPoint=(ylim[1]/maxSlope, ylim[1])
                print(maxTextPoint)
            else:
                maxTextPoint=(xlim[1], xlim[1]*maxSlope)
            if xlim[1]*minSlope > ylim[1]:
                minTextPoint=(ylim[1]/minSlope, ylim[1])
                print(minTextPoint)
            else:
                minTextPoint=(xlim[1], xlim[1]*minSlope)


            ax.plot([0, slopex], [0,maxSlope*slopex], **slopekmax)
            ax.annotate('dim=                         '.format(), xy=maxTextPoint, rotation = maxRotTrans, color=plotk['color'],  **textkmax)
            ax.annotate('    {:<5} upper slope={:>6.3} '.format(nparams, maxSlope), xy=maxTextPoint, rotation = maxRotTrans, color='black',  **textkmax)
           
            
            ax.plot([0, slopex], [0,minSlope*slopex], **slopekmin)
            ax.annotate('dim=                         '.format(), xy=minTextPoint, rotation = minRotTrans,color=plotk['color'], **textkmin)
            ax.annotate('    {:<5} lower slope={:>6.3} '.format(nparams, minSlope), xy=minTextPoint, rotation = minRotTrans,color='black', **textkmin)
            
            ax.plot([0, slopex], [0,maxSlope*slopex], **slopekout)
            ax.annotate('dim={:<5} upper slope={:>6.3} '.format(nparams, maxSlope), xy=maxTextPoint, rotation = maxRotTrans,  **textkout)
            ax.plot([0, slopex], [0,minSlope*slopex], **slopekout)
            ax.annotate('dim={:<5} lower slope={:>6.3} '.format(nparams, minSlope), xy=minTextPoint, rotation = minRotTrans, **textkout)
   
    
    if returnHandles: return fig, ax, handles
    else: return fig, ax


def histPlot(scoreDicts, infoDict = {}, scoreDictKeys=None, fig = None, ax=None, normalize=False, sqrtNorm=True, reverseAxis=False, histRange = None, labels=None, plotKwargs = {}, returnHandles=False, edge=False, **kwargs):
    if isinstance(scoreDicts, str):
        infoDict, scoreDicts, errDict = loadRunJson(scoreDicts)
    if isinstance(fig, type(None)) and isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    elif isinstance(ax, type(None)):
        ax = fig.add_subplot()
    
    handles = [] 
   
    doRange=False
    if isinstance(histRange, type(None)):
        histRange = [np.inf, -np.inf]
        doRange = True
    for i, scoreDict in enumerate([val for key, val in sorted(list(scoreDicts.items()), key = lambda x: int(x[0]))]): 
        scores= scoreDict['scores']  
        nparams= scoreDict['nparams']  
        
        if normalize and sqrtNorm: norm = np.sqrt(nparams)
        elif normalize and not sqrtNorm: norm = nparams
        else: norm = 1

        for j, tup in enumerate(scores.values()):
            dist, comp = tup
            if reverseAxis:
                val= comp
            else:
                val= dist
            if doRange:
                if val/norm < histRange[0]: histRange[0] = val/norm
                if val/norm > histRange[1]: histRange[1] = val/norm
    if not isinstance(scoreDictKeys, (type(None), list)):
        scoreDictKeys = [scoreDictKeys]

    for i, tup in enumerate([(key, val) for key, val in sorted(list(scoreDicts.items()), key = lambda x: int(x[0]))]): 
        key, scoreDict = tup
        if not isinstance(scoreDictKeys, type(None)) and key not in scoreDictKeys:
            continue
        scores= scoreDict['scores']  
        nparams= scoreDict['nparams']  
        Ne= scoreDict['Ne'] 

        vals = np.zeros(len(scores))
        for j, tup in enumerate(scores.values()):
            dist, comp = tup
            if reverseAxis:
                vals[j] = comp
            else:
                vals[j] = dist
                
        
        if normalize and sqrtNorm: norm = np.sqrt(nparams)
        elif normalize and not sqrtNorm: norm = nparams
        else: norm = 1
        
        plotk = {
                'bins':100,
                'histtype':'stepfilled',
                }
        plotk.update(kwargs)
        
        
        if not isinstance(labels, type(None)): plotk['label'] = labels[i]
        elif 'label' in scoreDict: plotk['label']= scoreDict['label']  

        if 'color' in scoreDict: plotk['color']= scoreDict['color']
        if 'hatch' in scoreDict: plotk['hatch']= scoreDict['hatch']
        if isinstance(plotKwargs, dict): plotk.update(plotKwargs)
        elif isinstance(plotKwargs, list): plotk.update(plotKwargs[i])
        

        if edge:
            plotke = plotk.copy()
            plotke.update({'histtype':'step', 'zorder':2})
            ax.hist(vals/norm, range=histRange, **plotke)
        A = ax.hist(vals/norm, range=histRange, **plotk)
        handles.extend(A[2])

    if reverseAxis:
        if 'compareMetric' in infoDict:  ax.set_xlabel(infoDict['compareMetric'])
    else:
        if 'distType' in infoDict: ax.set_xlabel(infoDict['distType'])
    
    ax.set_ylabel('Count')

    if returnHandles: return fig, ax, handles
    else: return fig, ax


def boxPlot(runDicts, infoDict = {}, scoreDictKeysList=[], fig = None, ax=None, normalize=False, sqrtNorm=True, vert=False, reverseAxis=False, labelList=[], plotKwargs = {}, returnHandles=False, boxColorsList=[], **kwargs):

    

    if not isinstance(runDicts, list):
        runDicts = [runDicts]
    #Expand each run into rundicts, each contain multiple scoredict
    
        


    if isinstance(fig, type(None)) and isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    elif isinstance(ax, type(None)):
        ax = fig.add_subplot()
    
    handles = [] 
   

    data = []
    datalabels = []
    datacolors = []
    for runi, scoreDicts in enumerate(runDicts): 
        try: 
            scoreDictKeys = scoreDictKeysList[runi]
        except IndexError:
            scoreDictKeys = None
        try: 
            labels = labelList[runi]
        except IndexError:
            labels = None
        
        try: 
            boxColors = boxColorsList[runi]
        except IndexError:
            boxColors = None

        if not isinstance(scoreDictKeys, (type(None), list)):
            scoreDictKeys = [scoreDictKeys]
        if isinstance(scoreDicts, str):
            infoDict, scoreDicts, errDict = loadRunJson(scoreDicts)
        for i, tup in enumerate([(key, val) for key, val in sorted(list(scoreDicts.items()), key = lambda x: int(x[0]))]): 
            key, scoreDict = tup
            if not isinstance(scoreDictKeys, type(None)) and key not in scoreDictKeys:
                continue
            scores= scoreDict['scores']  
            nparams= scoreDict['nparams']  
            Ne= scoreDict['Ne'] 

            vals = np.zeros(len(scores))
            for j, tup in enumerate(scores.values()):
                dist, comp = tup
                if reverseAxis:
                    vals[j] = comp
                else:
                    vals[j] = dist
                    
            
            if normalize and sqrtNorm: norm = np.sqrt(nparams)
            elif normalize and not sqrtNorm: norm = nparams
            else: norm = 1
            data.append(vals/norm)
            
            if not isinstance(labels, type(None)): datalabels.append(labels[i])
            elif 'label' in scoreDict: datalabels.append(scoreDict['label'])
            else: datalabels.append(None)

            if not isinstance(boxColors, type(None)): datacolors.append(boxColors[i])
            elif 'color' in scoreDict: datacolors.append(scoreDict['color'][0])
            else: datacolors.append(None)
           
    A = ax.boxplot(data, vert=vert, labels=datalabels, patch_artist=True)
    patches = A['boxes']
    medians = A['medians']
    for i, patch in enumerate(patches):
        patch.set_facecolor(datacolors[i])
        medians[i].set_color('black')
    if reverseAxis:
        if 'compareMetric' in infoDict:  axlabel = infoDict['compareMetric']
    else:
        if 'distType' in infoDict: axlabel = infoDict['distType']

    if vert:
        ax.set_ylabel(axlabel)
    else:
        ax.set_xlabel(axlabel)
    
    ax.set_ylabel('Count')

    if returnHandles: return fig, ax, A
    else: return fig, ax




def paramRanges(dimLo, dimHi, maxq, Nb, fig=None, ax=None, colorbar=True, legend=True, **kwargs):
    """Where maxparams is at least a list of [lowest dim, highest dim]"""
    maxparams = [dimLo, dimHi]

    if isinstance(fig, type(None)) and isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    elif isinstance(ax, type(None)):
        ax = fig.add_subplot()
    elif isinstance(fig, type(None)):
        fig = ax.get_figure()
    
    import descriptor_direct as desd
    
    axparam = ax.twinx()
    axparam.get_yaxis().set_visible(False)

    nameList = ['n0', 'n1', 'n2', 'l0', 'l1', 'l2', 'p']
    paramHandlesDict = {}
    maxparamList = []
    for val in range(int(maxparams[0]), int(maxparams[-1])):
        maxparamList.append(val)
        maxparamList.append(val+0.9)
    maxparamList = np.array(maxparamList)
    paramList = desd.get_parameters(maxq, Nb, maxp=np.inf, maxparam=maxparams[-1], lexiSort=True)
    upperParams = np.zeros(7)-np.inf
    lowerParams = np.zeros(7)+np.inf
    for tup, param in paramList:
        for i in range(7):
            upperParams[i] = max(upperParams[i], param[i]) #get the max q's of each
            lowerParams[i] = min(lowerParams[i], param[i]) #get the max q's of each
    #print(len(paramList[int(maxparams[0]):int(maxparams[-1])]), maxparams)
   
    normP = pltcolors.Normalize(vmin=np.min(lowerParams), vmax=np.max(upperParams))
    colormapScalarP = pltc.ScalarMappable(norm=normP)
    colormapP = colormapScalarP.get_cmap()
    if np.max(upperParams[0:6]) <= 10: 
        cmap = 'tab10'
        norm=pltcolors.Normalize(vmin=0, vmax=10)
        colormapScalar = pltc.ScalarMappable(cmap = cmap, norm=norm)
    elif np.max(upperParams[0:6]) <= 20: 
        cmap = 'tab20'
        norm=pltcolors.Normalize(vmin=0, vmax=20)
        colormapScalar = pltc.ScalarMappable(cmap = cmap, norm=norm)
    else: 
        norm = normP
        colormapScalar = colormapScalarP
    colormapScalarP.set_array([]) #For matplotlib <3.3.1
    colormapScalar.set_array([])
    colormap = colormapScalar.get_cmap()
    #colorlist = list(pltcolors.TABLEAU_COLORS)
    for i in range(7):
        for val in range(int(lowerParams[i]), int(upperParams[i])+1):
            if i >= 6: facecolor = colormapP(normP(val))
            else: facecolor = colormap(norm(val))
            where=[]
            #print(i, val, facecolor)
            for k, data in enumerate(paramList[int(maxparams[0]):int(maxparams[-1])]):
                tup, param = data
                #print(i, param, param[i], val, param[i] == val)
                if param[i] == val:
                    where.append(True)
                    where.append(True) #twice b/c half steps
                else:
                    where.append(False)
                    where.append(False) #twice b/c half steps
            if val < 0: hatch = '--'
            else: hatch = None
            collect = pltcol.BrokenBarHCollection.span_where(
                    maxparamList, 
                    ymin=i,
                    ymax=i+0.9,
                    where=where,
                    facecolor=facecolor,
                    hatch=hatch,
                    alpha = 0.5,
                    label = val,
                    zorder=0,
                    )
            axparam.add_collection(collect)
            #axparam.scatter(maxparamList[where], np.zeros(len(maxparamList[where]))+i, color=facecolor)
            if val not in paramHandlesDict and i < 6: paramHandlesDict[val] = collect
        axparam.text(maxparamList[0], i, '{}'.format(nameList[i]), fontsize='large', color='white')
   
    axparam.set_ylim(0, 30)
    axparam.legend(handles=list(paramHandlesDict.values()))
    if colorbar:
        fig.colorbar(colormapScalarP, ax = ax)

    return fig, ax, list(paramHandlesDict.values()), colormapScalarP

def errPlot(errDicts, infoDict = {}, scoreDicts={}, fig = None, ax=None, normType='std', labels=None, plotKwargs = None, returnHandles=False, sufficientLine=True, doParamRanges=False,colorbar=True, **kwargs):
    """normType can be 'std', 'normstd', 'normstdsqrt'"""
    if isinstance(errDicts, str):
        infoDict, scoreDicts, errDicts = loadRunJson(errDicts)

    if isinstance(fig, type(None)) and isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    elif isinstance(ax, type(None)):
        ax = fig.add_subplot()
    elif isinstance(fig, type(None)):
        fig = ax.get_figure()
    handles = []
   

    dimLo = np.inf
    dimHi = -np.inf
    qHi = 0
    if 'Nb' in list(scoreDicts.values())[0] and sufficientLine:
        Nb = list(scoreDicts.values())[0]['Nb']
        ax.axvline(3*Nb**2*(Nb-1), label='Conjecture 2.3\nsufficient '+ r'$\xi_p$', linestyle=':', color='k',) 


    for i, data in enumerate([(q, val) for q, val in sorted(list(errDicts.items()), key = lambda x: int(x[0]))]):
        q, errDict = data
        nparams= errDict['nparams']  
        scoreStd= errDict['scorestd']  
        scoreNormStd= errDict['scorenormstd']  
        scoreNormStdSqrt= errDict['scorenormstdsqrt']

        dimLo = min(dimLo, np.min(nparams))
        dimHi = max(dimHi, np.max(nparams))
        qHi = max(qHi, int(q))
        
        if normType=='std': vals = scoreStd
        elif normType=='normstd': vals = scoreNormStd
        elif normType=='normstdsqrt': vals = scoreNormStdSqrt
        elif normType=='stddiff': 
            try:
                vals = np.gradient(scoreStd)
            except ValueError:
                continue
        else: raise ValueError('errPlot(): Unknown normType {}'.format(normType))


        
        plotk = {'label':'q: {}'.format(q), 'marker':'o'}
        plotk.update(kwargs)
        
        if isinstance(plotKwargs, dict): plotk.update(plotKwargs)
        elif isinstance(plotKwargs, list): plotKwargs.update(plotKwargs[i])
        A = ax.plot(nparams, vals, **plotk) 
        handles.append(A[0])
  
    if doParamRanges:
        paramRanges(dimLo, dimHi, qHi, Nb, fig=fig, ax=ax,colorbar=colorbar, **kwargs)

    if normType=='std': ylabel = 'St. Dev. Descriptor Distances'
    elif normType=='normstd': ylabel = 'St. Dev. Descriptor Distances / Dimensions'
    elif normType=='normstdsqrt': ylabel = r'St. Dev. Descriptor Distances / $\sqrt{Dimensions}$' 
    elif normType=='stddiff': ylabel = 'Derivative of St. Dev of Descriptor Distances'
    xlabel = 'Descriptor Dimension'
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    

    if returnHandles: return fig, ax, handles
    else: return fig, ax




if __name__ == '__main__':
    import glob
    import string
    import sys


    fns = glob.glob('./plot-results/*.json')
    #print(fns)

    doFig1=True
    doFig2=True
    doFig3=True
    doFig4=True
    doFig5=True
    doFig6=True
    doScratch=False
    
    if len(sys.argv) > 1 and sys.argv[1] == 'scratch':
        doFig1 = doFig2 = doFig3 = doFig4 = doFig5 = doFig6 = False
        doScratch = True

    if doScratch:
        fig,ax = plt.subplots(1,1)
    

    colWidth = 9 #5
    if doFig1:
        fig1 = []
        axs1 = []
        #fig1, axs1 = plt.subplots(3,1, figsize=(5, 8),  tight_layout=True)
        #axs1 = axs1.flatten()
        fig, ax = plt.subplots(1,1, figsize=(colWidth, 3),  tight_layout=True)
        fig1.append(fig)
        axs1.append(ax)
        fig, ax = plt.subplots(1,1, figsize=(colWidth, 3),  tight_layout=True)
        fig1.append(fig)
        axs1.append(ax)
        fig, ax = plt.subplots(1,1, figsize=(colWidth, 3),  tight_layout=True)
        fig1.append(fig)
        axs1.append(ax)
        labels1 = [r'$\chi$ + vs $\chi-$',
                r'$\chi -$ vs $\chi+$',
                r'$\chi +$ vs $\chi+$',
                r'$\chi -$ vs $\chi-$',
                ]
    if doFig2:
        fig2, axs2 = plt.subplots(1,2, figsize=(colWidth,5),  tight_layout=False)
        axs2 = axs2.flatten()

    if doFig3:
        fig3, axs3 = plt.subplots(1,2, figsize=(colWidth, 4),  tight_layout=True)
        axs3 = axs3.flatten()
        labels3 = [r'$\sigma$: 0.1',r'$\sigma$: 0.05',r'$\sigma$: 0.01',r'$\sigma$: 0.005']
    
    if doFig4:
        fig4, axs4 = plt.subplots(1,1, figsize=(colWidth, 3),  tight_layout=True)
        axs4 = [axs4] 
    
    if doFig5:
        fig5, axs5 = plt.subplots(1,1, figsize=(colWidth, 3),  tight_layout=True)
        axs5 = [axs5] 
        bihandles = []
        labels5 = lambda theta: [r'$\chi$ + vs $\chi-$, $\sigma:{}$'.format(theta),
                                 r'$\chi -$ vs $\chi+$, $\sigma:{}$'.format(theta),
                                 r'$\chi +$ vs $\chi+$, $\sigma:{}$'.format(theta),
                                 r'$\chi -$ vs $\chi-$, $\sigma:{}$'.format(theta),
                                ]
    if doFig6:
        fig6, axs6 = plt.subplots(1,1, figsize=(colWidth, 5),  tight_layout=True)
        axs6 = [axs6] 
        bihandles = []
        labels6 = lambda theta: [r'$\chi$ + vs $\chi-$'+'\n'+r'$\sigma:{}$'.format(theta),
                                 r'$\chi -$ vs $\chi+$'+'\n'+r'$\sigma:{}$'.format(theta),
                                 r'$\chi +$ vs $\chi+$'+'\n'+r'$\sigma:{}$'.format(theta),
                                 r'$\chi -$ vs $\chi-$'+'\n'+r'$\sigma:{}$'.format(theta),
                                ]
    
    norm=pltcolors.Normalize(vmin=0, vmax=20)
    colormapScalar = pltc.ScalarMappable(cmap = 'tab20', norm=norm)
    cmap = colormapScalar.get_cmap()
    
    norm=pltcolors.Normalize(vmin=0, vmax=20)
    colormapScalarb = pltc.ScalarMappable(cmap = 'tab20b', norm=norm)
    cmapb = colormapScalarb.get_cmap()
    
    colormapScalarc = pltc.ScalarMappable(cmap = 'tab20c', norm=norm)
    cmapc = colormapScalarc.get_cmap()
    
    def setAlpha(c, a):
        c = list(c)
        cr = [0,0,0,0]
        cr[0:3] = c[0:3]
        cr[3] = a
        return tuple(cr)
    plotKwargsHist = [
            {'facecolor':setAlpha(cmap(8), 0), 'edgecolor':setAlpha(cmap(8), 1) ,'linestyle':'-'},
            {'facecolor':setAlpha(cmap(12), 0),'edgecolor':setAlpha(cmap(12), 1),'hatch':None, 'linestyle':'--'},
            {'facecolor':setAlpha(cmap(0), 0.5), 'edgecolor':setAlpha(cmap(0), 1) ,  'linestyle':'-'},
            {'facecolor':setAlpha(cmap(6), 0.5), 'edgecolor':setAlpha(cmap(6), 1) ,'hatch':'//', 'linestyle':'-'},
            ]

    plotKwargsHistRef = lambda off: [
            {'facecolor':setAlpha(cmap(0+off ), 0),'edgecolor':setAlpha(cmap(0+off ), 1),'linestyle':'-', 'hatch':None, 'zorder':4, 'linewidth':1.5},
            {'facecolor':setAlpha(cmap(0+off ), 0),'edgecolor':setAlpha(cmap(0+off ), 1),'linestyle':'-', 'hatch':None, 'zorder':4, 'linewidth':1.5},
            {'facecolor':setAlpha(cmap(0+off ), 0.3),'edgecolor':setAlpha(cmap(0+off ), 0.3),'linestyle':'-',  'hatch':None},
            {'facecolor':setAlpha(cmap(0+off ), 0.3),'edgecolor':setAlpha(cmap(0+off  ),0.3),'linestyle':'-',  'hatch':None},
            ]
    
    plotKwargsBoxRef = lambda off: [
            {'facecolor':setAlpha(cmap(0+off ), 1)},
            {'facecolor':setAlpha(cmap(0+off ), 1)},
            {'facecolor':setAlpha(cmap(0+off ), 1)},
            {'facecolor':setAlpha(cmap(0+off ), 1)},
            ]
    def rearrange(inList): #this is a really hacky way of doing it but it's only for this particular case
       tempList = []
       tempList.append(inList[1])
       tempList.append(inList[2])
       tempList.append(inList[0])
       return tempList
    #SET NORMALIZATION FOR THE WHOLE THING 
    normalize = True
    sqrtNorm = True

    if normalize and sqrtNorm: 
        distLabel = lambda d: r'Distance ({})'.format(d)+ ' / $\sqrt{dim}$'
        normType='normstdsqrt'
        normDir = 'normsqrt'
        histRange=[0, 3]
    elif normalize and not sqrtNorm: 
        distLabel = lambda d: r'Distance ({}) / dimension'.format(d)
        normType='normstd'
        normDir = 'norm'
        histRange = [0, 0.065]
    else: 
        distLabel= lambda d:'Distance ({})'.format(d)
        normType='std'
        normDir = 'raw'
        histRange = [0, 100]

    fnList = []
    labelList = []
    colorList = []
    scoreDictKeysList = []
    plot6NotDone = True

    for fn in fns:
        print(fn)
        #if 'counterBispectrumRef-0-' in fn:
        #    print('fig 1')
        #    ax = axs1[0]
        #    bullet = string.ascii_lowercase[0]
        #    histPlot(fn, bins=100, fig=fig1, ax=ax, labels=labels1)
        #    ax.set_title('{}) 4-Body Bispectrum (No perturbations)'.format(bullet), loc='left', fontsize='medium')
        if 'counterBispectrumRef-0_001-' in fn and doFig1:
            print('fig 1')
            ax = axs1[0]
            bullet = string.ascii_lowercase[0]
            histPlot(fn, bins=50, ax=ax, labels=labels1, plotKwargs=plotKwargsHist, edge=True, normalize=normalize, sqrtNorm=sqrtNorm)
            #boxPlot(fn, bins=50, ax=ax, labels=labels1, plotKwargs=plotKwargsHist, normalize=normalize, sqrtNorm=sqrtNorm)
            ax.set_title('{}) 4-Body Bispectrum'.format(bullet), loc='left', fontsize='medium')
            ax.set_xlabel(distLabel('L2'))
        
        
        

        if 'counterPlaneRef-0_001-' in fn and doFig1:
            print('fig 1')
            ax = axs1[1]
            bullet = string.ascii_lowercase[1]
            a,b,histHandles=histPlot(fn, bins=50, ax=ax, labels=labels1, plotKwargs = plotKwargsHist, edge=True, returnHandles=True, normalize=normalize, sqrtNorm=sqrtNorm)
            ax.set_title('{}) 3-Body Planar'.format(bullet), loc='left', fontsize='medium')
            ax.set_xlabel(distLabel('L2'))
        
        if 'counterRef-0_001-' in fn and doFig1:
            ax = axs1[2]
            print('fig 1')
            bullet = string.ascii_lowercase[2]
            histPlot(fn, bins=50, ax=ax, labels=labels1, plotKwargs = plotKwargsHist, edge=True, normalize=normalize, sqrtNorm=sqrtNorm)
            ax.set_title('{}) 3-Body Tetrahedral'.format(bullet), loc='left', fontsize='medium')
            ax.set_xlabel(distLabel('L2'))
        
        if 'a4-params-err' in fn and doFig2:
            print('fig 2')
            ax = axs2[0]
            bullet = string.ascii_lowercase[0]
            errPlot(fn, ax=ax, labels=None, normType=normType, doParamRanges=True)
            #ax.set_title('{}) 3-Atom Environment'.format(bullet), loc='left', fontsize='medium')
            ax.set_xlabel('Number of Dimensions') 
            ax.set_ylabel('St. Dev. Distances')
            ax.set_xticks([0,250,500,750,1000])
        if 'a5-params-err' in fn and doFig2:
            print('fig 2')
            ax = axs2[1]
            bullet = string.ascii_lowercase[1]
            errPlot(fn, ax=ax, labels=None, normType=normType, doParamRanges=True)
            #ax.set_title('{}) 4-Atom Environment'.format(bullet), loc='left', fontsize='medium')
            ax.set_xlabel('Number of Dimensions') 
            ax.set_ylabel(None) 
            ax.set_xticks([0,250,500,750,1000])
        
        if 'L1-des' in fn and doFig3:
            print('fig 3')
            ax = axs3[0]
            bullet = string.ascii_lowercase[0]
            smokePlot(fn, ax=ax, refs=[0], labels=labels3, normalize=normalize, sqrtNorm=sqrtNorm)
            #ax.set_title('{})'.format(bullet), loc='left', fontsize='medium')
            ax.set_ylabel(distLabel('L1'))
        
        if 'L2-des' in fn and doFig3:
            print('fig 3')
            ax = axs3[1]
            bullet = string.ascii_lowercase[1]
            smokePlot(fn, ax=ax, refs=[0], labels=labels3, normalize=normalize, sqrtNorm=sqrtNorm)
            #ax.set_title('{})'.format(bullet), loc='left', fontsize='medium')
            ax.set_ylabel('Descriptor Distance (L2)')
            ax.set_ylabel(distLabel('L2'))
        
        if 'a3-params-err.json' in fn and doFig4:
            print('fig 4')
            ax = axs4[0]
            bullet = string.ascii_lowercase[0]
            a, b, handles = smokePlot(fn, colormap='viridis', ax=ax, labels=None, refs=[0], returnHandles=True, colorbar=True, normalize=normalize, sqrtNorm=sqrtNorm)
            #ax.set_title('{}) 3-Atom Environment'.format(bullet), loc='left', fontsize='medium')
            #ax.set_ylabel('Descriptor Distance (L2)')
            ax.set_ylabel(distLabel('L2'))
            #plt.colorbar(handles[0])
        
        if 'counterBispectrumRef-0_001-' in fn and doFig5:
            print('fig 5')
            off=0
            ax = axs5[0]
            bullet = string.ascii_lowercase[0]
            a,b,handles = histPlot(fn, bins=50, ax=ax, labels=labels5(0.001), plotKwargs=plotKwargsHistRef(off), edge=False, histRange=histRange, returnHandles=True, scoreDictKeys=['0', '2'], normalize=normalize, sqrtNorm=sqrtNorm)
            bihandles.extend(handles)
            ax.set_xlabel(distLabel('L2'))
            ax.set_ylim(0, 75)
        
        if 'counterBispectrumRef-0_005-' in fn and doFig5:
            print('fig 5')
            off=2
            ax = axs5[0]
            bullet = string.ascii_lowercase[0]
            a,b,handles = histPlot(fn, bins=50, ax=ax, labels=labels5(0.005), plotKwargs=plotKwargsHistRef(off), edge=False, histRange=histRange, returnHandles=True, scoreDictKeys=['0', '2'], normalize=normalize, sqrtNorm=sqrtNorm)
            bihandles.extend(handles)
            ax.set_xlabel(distLabel('L2'))
            ax.set_ylim(0, 75)
        
        if 'counterBispectrumRef-0_01-' in fn and doFig5:
            print('fig 5')
            off=4
            ax = axs5[0]
            bullet = string.ascii_lowercase[0]
            a,b,handles = histPlot(fn, bins=50, ax=ax, labels=labels5(0.01), plotKwargs=plotKwargsHistRef(off), edge=False, histRange=histRange, returnHandles=True, scoreDictKeys=['0', '2'], normalize=normalize, sqrtNorm=sqrtNorm)
            bihandles.extend(handles)
            ax.set_xlabel(distLabel('L2'))
            ax.set_ylim(0, 75)

        if doFig6:

            ax = axs6[0]
            if 'counterBispectrumRef-0_001-' in fn:
                off=0
                fnList.append(fn)
                labelList.append(labels6(0.001))
                scoreDictKeysList.append(['0', '2'])
                colorList.append([dat['facecolor'] for dat in plotKwargsBoxRef(off)])
                
            if 'counterBispectrumRef-0_005-' in fn:
                off=2
                fnList.append(fn)
                labelList.append(labels6(0.005))
                scoreDictKeysList.append(['0', '2'])
                colorList.append([dat['facecolor'] for dat in plotKwargsBoxRef(off)])
            if 'counterBispectrumRef-0_01-' in fn: 
                off=4
                fnList.append(fn)
                labelList.append(labels6(0.01))
                scoreDictKeysList.append(['0', '2'])
                colorList.append([dat['facecolor'] for dat in plotKwargsBoxRef(off)])
            if plot6NotDone and len(fnList) == 3:
                a,b,handles = boxPlot(rearrange(fnList), ax=ax, labelList=rearrange(labelList), returnHandles=True, scoreDictKeysList=rearrange(scoreDictKeysList), normalize=normalize, sqrtNorm=sqrtNorm, boxColorsList=rearrange(colorList))
                plot6NotDone = False
            print('fig 6')
            bullet = string.ascii_lowercase[0]
            ax.set_xlabel(distLabel('L2'))

        if doScratch:
            print('scratch')
            if 'a4-params-err' in fn: 
                errPlot(fn, ax=ax, labels=None, normType='std', doParamRanges=True)
                ax.set_title('5-Atom Environment', loc='left', fontsize='medium')
            #if 'a3-params-err' in fn: 
            #if 'bimodal-20-err' in fn: 
            #    print('scratch')
            #    #errPlot(fn, ax=ax, labels=None, normType='stddiff', doParamRanges=True)
            #    fig, ax, handles = smokePlot(fn, ax=ax, labels=None, colormap='jet', refs=[0], returnHandles=True, colorbar=True)
            #    #fig.colorbar(handles[0])
    if doScratch:
        plt.show()
    
    rootDir = 'images/{}/'.format(normDir)
    try:
        os.mkdir(rootDir)
    except:
        pass

    if doFig1: 
        axs1[1].legend(handles=histHandles, loc='upper center')
        for i, fig in enumerate(fig1):
            fig.savefig('{}/fig1-{}.png'.format(rootDir, i), dpi=600) 
    if doFig2:
        axs2[1].legend(fontsize='small')
        fig2.savefig('{}/fig2.png'.format(rootDir), dpi=600) 
    if doFig3:
        axs3[1].legend()
        fig3.savefig('{}/fig3.png'.format(rootDir), dpi=600) 
    if doFig4:
        fig4.savefig('{}/fig4.png'.format(rootDir), dpi=600) 
    if doFig5:
        handles = []
        handles.append(bihandles[0])
        handles.append(bihandles[1])
        handles.append(bihandles[4])
        handles.append(bihandles[5])
        handles.append(bihandles[2])
        handles.append(bihandles[3])
        axs5[0].legend(handles=handles)
        fig5.savefig('{}/fig5.png'.format(rootDir), dpi=600) 
    if doFig6:
        fig6.savefig('{}/fig6.png'.format(rootDir), dpi=600) 
    #fn = 'peturb-30-des.json'
    #fig, ax = smokePlot(fn, colormap='viridis', refs = [0])
    ##fig, ax = histPlot(fn)
    #fn = 'peturb-30-err.json'
    #fig, ax = smokePlot(fn, colormap='viridis', refs = [0], fig=fig, ax=ax)
    ##fig, ax = errPlot(fn, normType = 'stddiff')
    

