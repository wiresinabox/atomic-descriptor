import numpy as np
import scipy.special as spf
import scipy.linalg as spl
from scipy.special import spherical_jn as sjn
import warnings

#Tabulated dictionaries to hopefully speed things up
PlmDict = {} #(l,m,x):Plm.
CGDict = {}
gnlDict = {}

def halley(l, lwr_bnd, upr_bnd, TOL1=2.2204e-13, TOL2=2.2204e-14, MAXIT=int(1e5)):
    x=(lwr_bnd + upr_bnd)/2.
    l1 = l+1

    for i in range(MAXIT):
        a = spf.spherical_jn(l, x)
        b = spf.spherical_jn(l1, x)

        if np.abs(a) < TOL2:
            break
        x2 = x*x
        #dx = -2*x*a*(l*a-x*b) / ((l*l1+x2)*a*a-2*x*(2*l+1)*a*b+2*x2*b*b)
        dx = -2*x*a*(l*a-x*b) / ((l*l1+x2)*a**2 - 2*x*(2*l+1)*a*b + 2*x2*b**2)
        if np.abs(dx) < TOL1:
            break

        if dx > 0:
            lwr_bnd = x
        else:
            upr_bnd = x
        
        if (upr_bnd-x) < dx or dx < (lwr_bnd-x):
            dx = (upr_bnd - lwr_bnd) / 2. - x;
        
        x=x+dx

    if i>MAXIT-1:
        warnings.warn('halley() failed to converge.')
    return x

def spherical_jn_zeros(n_max):
    """Roots of the spherical functions of the first kind. Returns a maxtrx of the first (l+1) nonzero roots of orders 0 <= l <= n_max.
    Returns: u_all an (n+2, n+1) size matrix of [n,l]"""
    
    u_all = np.zeros((n_max+2, n_max+1))
    #zeros of j_0 = sin(x)/x
    u_all[:,0] = np.pi * np.arange(1,n_max +3)
    
    #Halley's Method
    for l  in range(1, n_max+1):
        for n in range(0, n_max-l+2):
            u_all[n,l] = halley(l, u_all[n,l-1], u_all[n+1,l-1])
    return u_all

def get_gnl(rc, n_max, r, saveCalc = True):
    """Direct reimplementation for calculating gnl"""
    #print('rc: {}, n_max: {}, r: {}'.format(rc, n_max, r))
    key = (rc, n_max, r)
    if isinstance(r, list) or isinstance(r, tuple):
        r = np.array(r)
    elif not isinstance(r, np.ndarray):
        r = np.array([r])

    if saveCalc and key in gnlDict:
        return gnlDict[key]
    else:
        u_all=spherical_jn_zeros(n_max)
        #coefficients of fnl
        coeff_a = np.zeros((n_max+1, n_max+1))
        coeff_b = np.zeros((n_max+1, n_max+1))
        for l in range(0,n_max+1):
            u = u_all[:, l]
            for n in range(0, (n_max-l+1)):
                c=np.sqrt(2/(rc**3*(u[n]**2+u[n+1]**2)))
                coeff_a[n,l]=u[n+1]/ sjn(l+1,u[n]) * c
                coeff_b[n,l]=-u[n] / sjn(l+1, u[n+1]) * c
        #print('coeff_a', coeff_a)
        #print('coeff_b', coeff_b)
        e = np.zeros(n_max+1)
        d=np.ones(n_max+1)
        nu = len(r);
        gnl = np.zeros((nu, n_max+1, n_max+1))
        arg = r/rc
        for l in range(0, n_max+1):
            #print('uall', u_all, l)
            u = u_all[:,l]
            for n in range(1, n_max-l+1):
                e[n] = (u[n-1]**2 * u[n+1]**2) / ((u[n-1]**2 + u[n]**2) * (u[n]**2 + u[n+1]**2))
                d[n] = 1. - e[n] / d[n-1]
            #fnl
            for n in range(0, n_max-l+1):
                for a in range(0, nu):
                    gnl[a,n,l] = coeff_a[n,l] * sjn(l,u[n]*arg[a]) + coeff_b[n,l] * sjn(l,u[n+1]*arg[a])
                    #print('gnl:', gnl, coeff_a[n,l], sjn(l,u[n]*arg[a]), coeff_b[n,l], sjn(l,u[n+1]*arg[a]))
                    #print('l:{}, u[n]*arg[a]:{}, u[n+1]*arg[a]:{}'.format(l,u[n]*arg[a],u[n+1]*arg[a]))
            #convert to gnl
            for n in range(1, n_max-l+1):

                gnl[:,n,l]=(gnl[:,n,l]+np.sqrt(1.-d[n])*gnl[:,n-1,l])/np.sqrt(d[n])
        if saveCalc:
            gnlDict[key] = gnl
        return gnl



def clebsch_gordan(j1, j2,j,m, saveCalc=True):
    global CGDict

    if not np.isscalar(j1): raise TypeError("j1 must be a scalar.")
    if not np.isscalar(j2): raise TypeError("j2 must be a scalar.")
    if not np.isscalar(j): raise TypeError("j must be a scalar.")
    if not np.isscalar(m): raise TypeError("m must be a scalar.")

    if np.any(np.array([j1,j2,j]) < 0) or np.any(np.array([j1,j2,j]) % 0.5 != 0) or (j+m)%1 != 0:
        raise ValueError('j1 = {}, j2 ={}, j={} must be non-negative and integers or half-integers. j+m={} must be an integer.'.format(j1,j2,j,j+m))

    #print( j < np.abs(j1-j2), j>j1+j2, np.abs(m) > j)
    if j < np.abs(j1-j2) or j > j1+j2 or np.abs(m) > j:
        #base case
        m1 = []
        m2 =[]
        C = []
        return C,m1,m2

    key = (j1, j2, j, m)
    if saveCalc and key in CGDict:
        C, m1, m2 = CGDict[key]
        return C, m1, m2
    else:
        m1l = (m - j1 - j2 + np.abs(j1 - j2 + m)) / 2;
        m1n = (m + j1 + j2 - np.abs(j1 - j2 - m)) / 2;

        m1 = np.arange(m1l,m1n+1)
        m2 = m - m1

        j_const = j1 * (j1 + 1) + j2 * (j2+1) - j * (j+1)
        n=int(m1n - m1l+1)

        A = np.zeros((n,n))
        for a in range(0,n):
            A[a,a] = j_const + 2 * m1[a] * m2[a]

        for a in range(0, n-1):
            tmp = np.sqrt(j1*(j1+1)-m1[a]*m1[a+1])*np.sqrt(j2*(j2+1)-m2[a]*m2[a+1])
            A[a,a+1] = tmp
            A[a+1,a] = tmp

        C = spl.null_space(A)
        C = np.sign(C[n-1]) * C / np.sqrt(np.matmul(C.transpose(), C))
        
        if saveCalc:
            CGDict[key] = (C.flatten(), m1, m2)
        return C.flatten(), m1, m2


def assoc_legendre(l,m,x, maxiter=1e5, saveCalc=True): #AKA Plm
    eps = np.finfo(1.0).eps 
    key = (l,m,x)
    
    global Pdict
    if saveCalc and key in PlmDict:
        return PlmDict[key]
    else:
        if np.abs(m) < eps:
            Pm = 1.
        else:
            Pm = (-1)**m * np.prod(np.arange(m, 2*m)) / 2**(m-1) * (1- x * x)**(m/2)

        if np.abs(l-m) < eps:
            Plm = Pm
            return Plm

        Po = x * (2*m+1) *Pm
        l1 = m+1
        i = 0
        while np.abs(l-l1) > eps and i<maxiter:
            Pp=((2*l1+1)*x*Po-(l1+m)*Pm)/(l1-m+1)
            Pm=Po
            Po=Pp
            l1+=1
            i+=1
        if i >= maxiter:
            print('assoc_legendre: hit maxiter')
            print('l:{}, m:{}, x:{}'.format(l,m,x))
        Plm=Po
        if saveCalc:
            PlmDict[key] = Plm
        return Plm


def get_sph(l, m, theta, phi):
    #Spherical harmonics
    absm = np.abs(m)

    p = assoc_legendre(l, absm, np.cos(theta))
    p = p/np.sqrt(np.prod(np.arange((l-absm+1),(l+absm+1))))
    if m < 0:
        p=(-1)**absm * p
    Y = np.sqrt((2*l+1)/(4*np.pi)) * p * np.exp(complex(0,1)*m*phi)
    return Y
    
    #temp = np.sqrt((2*l+1)/(4*np.pi)) * np.sqrt((spf.factorial(l-absm)/spf.factorial(l+absm))) * assoc_legendre(l,absm,np.cos(theta))*np.exp(complex(0,1)*absm*phi)
    #print(np.sqrt((2*l+1)/(4*np.pi)), np.sqrt((spf.factorial(l-m)/spf.factorial(l+m)))* assoc_legendre(l,m,np.cos(theta)), np.exp(complex(0,1)*m*phi))
    #print(np.sqrt((2*l+1)/(4*np.pi)), p, np.exp(complex(0,1)*m*phi))
    #print(l, m, 'temp', temp, 'Y', Y, np.sqrt(((temp-Y)**2)) > 1e-8)

def get_Psinlm(rc, n, l, m, r):
    gnl = get_gnl(rc, n+l, r[0])
    g = gnl[0, n, l]
    Yml = get_sph(l,m,r[1], r[2]) 
    #print('g:', g, 'Yml', Yml)
    return g*Yml

def get_z(rc,n0,n1,n2,l0,l1,l2,r0,r1,r2,m0,m1,m2):
    psi0 = np.conjugate(get_Psinlm(rc, n0, l0, m0, r0))
    psi1 = get_Psinlm(rc, n1, l1, m1, r1)
    psi2 = get_Psinlm(rc, n2, l2, m2, r2)
    return psi0*psi1*psi2


def get_chi(rc, n0,n1,n2,l0,l1,l2,r0,r1,r2):
    """Non explicit, possibliy imaginary and slower version of chi. Equation 5"""
    coeff = (-1)**l0 / np.sqrt(2*l0+1)
    
    gnl = get_gnl(rc, n0+l0, r0[0])
    g0=gnl[0,n0,l0]
    gnl = get_gnl(rc, n1+l1, r1[0])
    g1=gnl[0,n1,l1]
    gnl = get_gnl(rc, n2+l2, r2[0])
    g2=gnl[0,n2,l2]

    X = 0
    for m0 in range(-l0, l0+1):
        C,m1,m2=clebsch_gordan(l1,l2,l0,m0)
        for a in range(0, np.size(C)):
            Y0 = np.conjugate(get_sph(l0, m0, r0[1], r0[2]))
            Y1 = get_sph(l1, m1[a], r1[1], r1[2])
            Y2 = get_sph(l2, m2[a], r2[1], r2[2])
            X  = X + C[a] * Y0 * Y1 * Y2

    return coeff *g0 *g1 *g2 * X

def get_chi2(rc, n0,n1,n2,l0,l1,l2,r0,r1,r2):
    """The explicit formulation of X. l0+l1+l2 must be even."""
    #print('rc:{}\nn0:{}\nn1:{}\nn2:{}\nl0:{}\nl1:{}\nl2:{}\nr0={}\nr1={}\nr2={}'.format(rc,n0,n1,n2,l0,l1,l2,r0,r1,r2))
    gnl = get_gnl(rc, n0+l0, r0[0])
    g0=gnl[0,n0,l0]
    gnl = get_gnl(rc, n1+l1, r1[0])
    g1=gnl[0,n1,l1]
    #print('hello', rc, n1+l1, r1[0], gnl)
    
    gnl = get_gnl(rc, n2+l2, r2[0])
    g2=gnl[0,n2,l2]

    X = 0
    
    for m0 in np.arange(-l0, l0+1):
        abs_m0=np.abs(m0)
        P0=assoc_legendre(l0, abs_m0, np.cos(r0[1]))
        P0=P0*np.sqrt((2*l0+1)/(2*np.prod(np.arange(l0-abs_m0+1, l0+abs_m0+1))))
        
        if m0 < 0:
            P0=(-1)**abs_m0 * P0

        C,m1,m2=clebsch_gordan(l1,l2,l0,m0)
        for a in range(0, np.size(C)):
            abs_m1 = np.abs(m1[a])
            P1 = assoc_legendre(l1, abs_m1, np.cos(r1[1]))
            P1 = P1 * np.sqrt((2*l1+1)/(2*np.prod(np.arange((l1-abs_m1+1), (l1+abs_m1+1)))))
            
            if m1[a] < 0:
                P1 = (-1)**abs_m1 * P1

            abs_m2 = np.abs(m2[a])
            P2 = assoc_legendre(l2, abs_m2, np.cos(r2[1]))
            P2 = P2 * np.sqrt((2*l2+1)/(2*np.prod(np.arange(l2-abs_m2+1, l2+abs_m2+1))))
            if m2[a] < 0:
                P2 = (-1)**abs_m2 * P2

            X = X + C[a] *P0 *P1 * P2 *np.cos(-m0*r0[2] +m1[a] *r1[2] + m2[a] *r2[2])

    #print('X', X, g0, g1, g2)
    #print((-1)**l0, np.sqrt(2*l0+1), (2*np.pi)**1.5 * g0 *g1 * g2 * X)
    X = (-1)**l0 / np.sqrt(2*l0+1) / (2*np.pi)**1.5 * g0 *g1 * g2 * X
    return X

def cart2sph(cart):
    """converts the (x,y,z) tuple of values into (r, pol, azm)"""
    if not isinstance(cart, np.ndarray):
        cart = np.array(cart)
    if np.all(cart == 0):
        return np.array([0,0,0])
    r = np.sqrt(np.sum(cart**2) )

    pol = np.arccos(cart[2] / r) #0 to np.pi/2
    if pol == 0:
        azm = 0
    else:
        if np.abs((cart[0] / (r * np.sin(pol))) - 1) < 1e-8: 
            azm = np.arccos(1)
        elif np.abs((cart[0] / (r * np.sin(pol))) + 1) < 1e-8: 
            azm = np.arccos(-1)
        else: 
            azm = np.arccos(cart[0] / (r * np.sin(pol)));
        if cart[1] < 0:
            azm = 2 * np.pi - azm
    sph = np.array([r, pol, azm])
    #print(cart, r, pol, azm)
    return sph

def sph2cart(sph):
    """converts (r, theta, phi) into (x, y, z)"""
    if not isinstance(sph, np.ndarray): sph = np.array(sph)
    r, pol,azm = sph
    x = r*np.sin(pol)*np.cos(azm)
    y = r*np.sin(pol)*np.sin(azm)
    z = r*np.cos(pol)
    cart = np.array([x, y, z])
    return cart


def get_Eij(i,j, rc, points, n0,n1,n2,l0,l1,l2, explicit=True):
    Eij = 0
    ri = points[i]
    rj = points[j]
    for k in [k for k in range(len(points)) if k != i or k != j]:
        rk = points[k]
        if explicit:
            out = get_chi2(rc, n0, n1, n2, l0, l1, l2, ri, rj, rk)
        else:
            #out2 = get_chi2(rc, n0, n1, n2, l0, l1, l2, ri, rj, rk)
            out = np.real(get_chi(rc, n0, n1, n2, l0, l1, l2, ri, rj, rk)) #This should be complex conjugate of chi. eq 6
            #print(out, out2)
        Eij += out
    #print('i,j: {},{} Eij:{}'.format(i,j,Eij))
    return Eij

def Eij_var_calc(i,j, rc, points, n0,n1,n2,l0,l1,l2, explicit=True):
    Eij = 0
    ri = points[i]
    rj = points[j]
    
    coeff = 1/(2*l0+1)
    for k in [k for k in range(len(points)) if k != i or k != j]:
        rk = points[k]
        for m0 in range(-l0, l0+1):
            C,m1,m2=clebsch_gordan(l1,l2,l0,m0)
            for a in range(0, np.size(C)):
                pass

        Eij += out
    #print('i,j: {},{} Eij:{}'.format(i,j,Eij))
    return Eij
    


def get_parameters(q, Nb, maxp = np.inf, maxparam=np.inf, lexiSort=True, prevQ=True):
    """returns a list of two tuples. The first are (q,r,s,t,u,v,p). The second are (n0,n1,n2,l0,l1,l2,p). Pairs are sorted lexicographically by the first tuple"""
    tupList = []
    i=0
    hiQ = q
    if prevQ: lowQ = 0
    else: lowQ = q
    #for q in range(lowQ, hiQ+1): 
    #    #for r in range(0, int(np.floor(q/2))+1):
    #    for r in range(0, q+1): 
    #        #for s in range(0, int(q-2*r)+1):
    #        for s in range(0, q-r+1):
    #            for t in range(0, s+1):
    #                #for u in range(0, int(2*r)+1):
    #                for u in range(0, r+1):
    #                #for u in range(0, int(2*r)+1):
    #                    for v in range(0, r-u+1):
    #                    #for v in range(0, int(2*r)-u+1):
    #                        for p in range(1, min(maxp, int(Nb**2))+1):
    #                            #The open question is how many values of p are needed to fully describe the system?
    #                            
    #                            tup= (q,r,s,t,u,v,p)
    #                            #Revised ordering
    #                            #n0 = int(q - 2*r - s)
    #                            #n1 = s-t
    #                            #n2 = t
    #                            #l0 = int(2*r)-u
    #                            #l1 = u-v
    #                            #l2 = v

    #                            #n0 = int(q-2*r-s)
    #                            n0 = int(q-r-s)
    #                            n1=s-t
    #                            n2 =t
    #                            l0=r-u
    #                            l1=r-v
    #                            l2=u+v
    #                            param=(n0,n1,n2,l0,l1,l2,p)
    #                            #print(tup, param)
    #                            if i < maxparam:
    #                                tupList.append([tup, param])
    #                                i+=1
    #                            else:
    #                                break
    #                    if i >= maxparam: break
    #                if i >= maxparam: break
    #            if i >= maxparam: break
    #        if i >= maxparam: break

    for q in range(lowQ, hiQ + 1): 
        for r in range(q, int(np.ceil(q / 2)) - 1, -1): 
            for s in range(min(2 * r - q, 1), -1, -1):
                for t in range(0, r + (q - 2 * r) * s + 1):
                    for u in range(0, t + 1):
                        for v in range(0, q - r + (2 * r - q) * s + 1):
                            for w in range(0, q - r + (2 * r - q) * s - v + 1):
                                for p in range(1, min(maxp, int(Nb**2)) + 1):
                                    #The open question is how many values of p are needed to fully describe the system?
                                
                                    tup = (q,r,s,t,u,v,w,p)
                                
                                    n0 = r + (q - 2 * r) * s - t
                                    n1 = t - u
                                    n2 = u
                                    l0 = q - r + (2 * r - q) * s - v
                                    l1 = q - r + (2 * r - q) * s - w
                                    l2 = v + w
                                    
                                    param=(n0,n1,n2,l0,l1,l2,p)
                                    
                                    #print(tup, param)
                                    if i < maxparam:
                                        tupList.append([tup, param])
                                        i+=1
                                    else:
                                        break
                            if i >= maxparam: break
                        if i >= maxparam: break
                    if i >= maxparam: break
                if i >= maxparam: break
            if i >= maxparam: break
    

    if lexiSort:
        sorted(tupList, key=lambda x: x[0])
    #print(tupList)
    return tupList

def get_chi_descriptor(rc, points, q, Nb, Ne, parameters=None, maxp=np.inf, maxparam=np.inf, lexiSort=True, verbose=True, explicit=True):
    """A less powerful 4-body descriptor used to test the proper one in counterexamples
    In this case, only 3(N-1) descriptors might need to be used
    Also we don't need p? 
    """
    print('Calculating Chi Descriptors') 
    
    if isinstance(parameters, type(None)):
        tupList = get_parameters(q, Nb, maxp=1, maxparam=maxparam, lexiSort=lexiSort)
    else:
        tupList = parameters        
    
    chiArray = np.zeros(len(tupList))

    #I think the general principle of these descriptors is to expand the delta function
    #Each entry of the descriptor is a coefficient of the expanded function
    #Each of these coefficients contains all the relevant points but varies by the frequency of the expansion function
    #In this case this these are the spherical harmonics. 3 of them in fact.
    #And each one varies by an n and an l
    
    #The difference between this one and the proper one is that this one only completely describes
    #the set of tetrahedra (pi, pj, pk, and the center)
    #The proper descriptor adds the addtional parameter p
    #Now p has a maximum value of Nb**2, which is the number of (i,j) tuples that are used as 'reference points on the two axis'
    #This additional information allows it to solve 4 body counterexamples w/o 5 body information.
    if verbose:
        for p in points:
            print(sph2cart(p))
    for u in range(len(tupList)):
        tup, param = tupList[u]
        n0,n1,n2,l0,l1,l2,p = param
        
        for i in range(len(points)):
            for j in range(len(points)):
                for k in range(len(points)):
                    pi = points[i]
                    pj = points[j]
                    pk = points[k]
                    if explicit:
                        chi = get_chi2(rc, n0, n1, n2, l0, l1, l2, pi, pj, pk)
                    else:
                        chi = np.real(get_chi(rc, n0, n1, n2, l0, l1, l2, pi, pj, pk)) 
                    chiArray[u] += chi
        if verbose:
            print('k:{}/{} | (qrstuvp) {} | (n,l,p) {} | n0,n1,n2:{},{},{} | l0,l1,l2: {}+{}+{}={}. Is even? {} | {}'.format(k, len(tupList)-1, tup, param, n0,n1,n2, l0,l1,l2,l0+l1+l2,(l0+l1+l2)%2==0, chiArray[u]))
    return chiArray, []

def get_descriptor(rc, points, q, Nb, Ne, parameters=None, maxp=np.inf, maxparam=np.inf, lexiSort=True, verbose=True, explicit=True, **kwargs):
    """Where q is an integer that defines how many 7-tuples of parameters are used and how long the descriptor is.
    Nb is the upper limit on the expected number of atoms.
    Ne is the expected number of atoms.
    rc is the critical radius.
    points is a list of 3-tuples of points in spherical coordinates relative to the center point of rc"""
    if isinstance(parameters, type(None)):
        tupList = get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=lexiSort, **kwargs)
    else:
        tupList = parameters        
            ##Note that chi seems to go to zero when n1 or n2 is equal to zero
    if verbose:
        print('q: {}, Ne: {}, Nb: {}, descript len: {}'.format(q, Ne, Nb, len(tupList)))
    desVect = np.zeros(len(tupList))
    EijList = []
    #n0,n1,n2,l0,l1,l2,p = tupList[0][1]

    prevParam = None
    for k in range(len(tupList)):
        EijLocList = []
        tup, param = tupList[k]
        n0,n1,n2,l0,l1,l2,p = param
        #p = param[-1]
        Hp = spf.hermitenorm(p) #Note, this is using the probabilist's Hermite polynomial solely b/c it gives nicer numbers.
        if isinstance(prevParam, type(None)) or prevParam[:-1] != param[:-1]: #Reduce number of Eij calcs if only p is changing
            EijArray = np.zeros((len(points), len(points)))
            prevParam = param
            #print('calc Eij', tup, param)
            for i in range(len(points)):
                for j in range(len(points)):
                    EijArray[i,j] = get_Eij(i,j,rc,points,n0,n1,n2,l0,l1,l2, explicit=explicit)


        desCoeff = ((np.sqrt(2*np.pi)*spf.factorial(p))**(-1/2))
        hcoeff = 1/np.std(EijArray)
        #print('desCoeff:', desCoeff, 'hcoeff:', hcoeff, np.std(EijArray))
        #print(EijArray)
        for i in range(len(points)):
            for j in [j for j in range(len(points)) if not i ==j]:
                #hcoeff = (((4*np.pi*rc**3)/3)**(1/2))/(Ne**(1/2))
                #hcoeff = (((4*np.pi*rc**3)/3)**(3/2))/(Ne**(1/2))
                #Eij=get_Eij(i,j,rc,points,n0,n1,n2,l0,l1,l2, explicit=explicit)
                #desVect[k] += desCoeff * spf.eval_legendre(p, hcoeff*Eij)
                #desVect[k] += Eij#+= ((np.sqrt(2*np.pi)*spf.factorial(p))**(-1/2)) * spf.eval_legendre(p, hcoeff*Eij)
                #desVect[k] += ((np.sqrt(2*np.pi)*spf.factorial(p))**(-1/2)) * spf.eval_laguerre(p, hcoeff*Eij)
                #desVect[k] += ((np.sqrt(2*np.pi)*spf.factorial(p))**(-1/2)) * spf.eval_legendre(p, hcoeff*Eij)
                
                Eij=EijArray[i,j]
                desVect[k] += desCoeff * np.exp(-(hcoeff*Eij)**2/4) * Hp(hcoeff*Eij)
                #print(desCoeff, np.exp(-(hcoeff*Eij)**2/4), Hp(hcoeff*Eij))
                EijList.append(Eij*hcoeff)
                #EijLocList.append(Eij*hcoeff)

                #print(i,j, desVect[k],Hp(hcoeff*get_Eij(i,j,rc,points,n0,n1,n2,l0,l1,l2)), hcoeff, get_Eij(i,j,rc,points,n0,n1,n2,l0,l1,l2), hcoeff*get_Eij(i,j,rc,points,n0,n1,n2,l0,l1,l2))
        
        #print(EijLocList)
        #print('Variance', np.var(np.array(EijLocList)), 'vs', ((4*np.pi*rc**3)**(-3))*len(points))
        if verbose:
            print('k:{}/{} | (qrstuvp) {} | (n,l,p) {} | n0,n1,n2:{},{},{} | l0,l1,l2: {}+{}+{}={}. Is even? {} | {}'.format(k, len(tupList)-1, tup, param, n0,n1,n2, l0,l1,l2,l0+l1+l2,(l0+l1+l2)%2==0, desVect[k]))
        if np.isnan(desVect[k]):
            print('k:{}/{} | (qrstuvp) {} | (n,l,p) {} | n0,n1,n2:{},{},{} | l0,l1,l2: {}+{}+{}={}. Is even? {} | {}'.format(k, len(tupList)-1, tup, param, n0,n1,n2, l0,l1,l2,l0+l1+l2,(l0+l1+l2)%2==0, desVect[k]))
            for i, point in enumerate(points):
               print(i, sph2cart(point))
    EijList = np.array(EijList)
    #print(EijList)
    
    #print('Variance All', np.var(EijList))

    return desVect, EijList
    

