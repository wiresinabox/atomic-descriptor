import sys
import numpy as np
import os

if os.path.isdir('/home/esuwandi/bin'):
    pathToCRGW='/home/esuwandi/bin/'
    sys.path.append(pathToCRGW)
    from ae_dist import Ae_dist
    ae_dist_fun=Ae_dist('libaedist.so', '/home/esuwandi/bin')
else:
    pathToCRGW='../crgw/ae_dist/python'
    sys.path.append(pathToCRGW)
    from ae_dist import Ae_dist
    ae_dist_fun=Ae_dist('libaedist.so', '/usr/local/lib')

#spherical distance. points are in (r, pol, azm)
sph_dist = lambda p1, p2: np.sqrt(p1[0]**2 + p2[0]**2 - (2*p1[0]*p2[0]*(np.sin(p1[1])*np.sin(p2[1])*np.cos(p1[2]-p2[2])+np.cos(p1[1])*np.cos(p2[1]))))

def get_ae_dist(rc1, points1, rc2, points2, verbose=False):
    """Where rc is the critical point to the center, and points is a list of tuples or lists of each point in spherical coords"""
    n1 = len(points1)
    n2 = len(points2)

    #Types of the points. Assumed single composition.
    t1 = np.zeros(n1, dtype=np.uint32, order='F')
    t2 = np.zeros(n2, dtype=np.uint32, order='F')
    
    #Distance of point to the boundary
    l1 = np.array([rc1-coords[0] for coords in points1], dtype=np.float64, order='F')
    l2 = np.array([rc2-coords[0] for coords in points2], dtype=np.float64, order='F')
    D1 = np.zeros((n1,n1), dtype=np.float64, order='F')
    D2 = np.zeros((n2,n2), dtype=np.float64, order='F')
    #Euclidian distance between each point pair
    for i in range(n1):
        for j in range(n1):
            if i == j: D1[i,j]=0
            else: D1[i,j] = sph_dist(points1[i], points1[j])
            #print(i,j, points1[i], points1[j], D1[i,j])
    for i in range(n2):
        for j in range(n2):
            if i == j: D2[i,j]=0
            else: D2[i,j] = sph_dist(points2[i], points2[j])
    
    dist, G = ae_dist_fun(t1, t2, l1, l2, D1, D2)
    if verbose:
        print('dist:', dist) 
    return dist, G


if __name__ == '__main__':
    from test import gen_random_env
    
    rc=5
    points1 = gen_random_env(rc, N=3, useLocalSeed=True, seed=1)
    points2 = gen_random_env(rc, N=5, useLocalSeed=True, seed=None)
    print(points1)
    print(points2)
    print(get_ae_dist(rc, points1, rc, points2))
