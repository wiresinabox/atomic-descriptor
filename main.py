import descriptor_direct as desd
import numpy as np
import test
import matplotlib
defaultBack = matplotlib.get_backend()
print('Default plotting backend:', defaultBack)
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.gridspec as pltgs
import matplotlib.collections as pltcol
import matplotlib.cm as pltcm
import matplotlib.colors as pltcolors
import sys
import time
import argparse
from importlib import reload
import mputil as mpu
import plotutil as pltu
import os
parser = argparse.ArgumentParser()
parser.add_argument('-n', type=int, default=None, help='Number of subprocesses spawned. Default: None')
parser.add_argument('-q', type=int, default=2, help='q')
parser.add_argument('-p', type=str, default='max', help='p, if p=max: 0 < p < Nb**2, if p=half: 0 < p < Nb, if p=min, maxparam is set to 3*(Nb-1)')
parser.add_argument('-j', default=None , help='Job Id')
parser.add_argument('-t', default='lattice', help='Type of Job. Currently "env", "var", "mpTest". Default=doDes(-t)')
parser.add_argument('-o', default='.' , help='Output Directory')
parser.add_argument('-i', action='store_true', help='Display Interactive Plot')
parser.add_argument('-d', default='L2' , help='Descriptor Distance Metric. Choice: L1, L2, 0 (makes it zero)')
parser.add_argument('-c', default='GW' , help='Comparison Metric. Choice: L1, L2, GW, G2, G3, SOAP, chi (uses eq. 5)')
parser.add_argument('-std',  nargs = '+', type=float, default=None , help='Standard Deviations. Multiple entries means multiple runs')
parser.add_argument('-refs',  nargs = '+', type=int, default=None , help='References')
parser.add_argument('-seed', type=int, default=None , help='Seed for number of environment selection, scatter. Atomic environment placements will be random but consistent between runs if set.')
parser.add_argument('-envseed', type=int, default=None , help='Seed for atomic environment position. Positions all be the same if set.')
parser.add_argument('-a', type=int, default=3, help='atomsNum. Really only used for the random environment generator')
parser.add_argument('-lo', type=int, default=None, help='For err. Sets the low end of the error testing range')
parser.add_argument('-hi', type=int, default=None, help='For err. Sets the high end of the error testing range')
parser.add_argument('-num', type=int, default=10, help='For err, params. Sets how many steps are taken')
parser.add_argument('-step', type=int, default=1, help='For err, q. Sets how far each step is')
parser.add_argument('-short', action='store_true', help='For err, q. Calculates one set of descriptor and slices off sections of it instead of recalculating the entire thing.')
parser.add_argument('-save', action='store_true', help='Saves the json file')
parser.add_argument('-norm', default=None, help='How to normalize scores in plotting')
parser.add_argument('-nosub', action='store_true', default=False, help='Does not overwrite q if below Conjecture 2.3')
parser.add_argument('envNum', nargs='?',  default=2, type=int, help='Number of Environments')
args = parser.parse_args(sys.argv[1:])

if isinstance(args.n, int):
    cores = args.n
    mp = True
else:
    cores = 1
    mp = False
jobId = args.j
outDir = args.o
n = args.envNum
q = args.q
maxp = args.p
atomsNum = args.a
if isinstance(jobId, type(None)):
    jobId = '{}-{}-{}'.format(args.t, n, time.asctime().replace(':',' ').replace(' ', '-'))

def envPlot(genType='random'):
    global atomsNum
    fig = plt.figure() 
    ax = fig.add_subplot(projection='3d') 
    rc=1
    rcenv = 1
    
    offset=[0,0,0]
    seed=1

    latticeCoeff=[0.8,0.8,1.5]
    stds = [0.1, 0.05, 0.01, 0.005]
    stds = [0, 0, 0, 0]
    envLists = []
    if genType == 'lattice':
        for std in stds:
            print('std:', std)
            envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, offsetxyz = offset, genType=genType, latticeCoeff = latticeCoeff, std=std, sph=False, center=True)
            envLists.append((envList, 'std={}, Ne={}'.format(std, Ne)))
    else: 
        envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, offsetxyz = offset, genType=genType, sph=False) 
        #envList = np.array(envList)
        envLists.append((envList, 'Ne={}'.format(Ne)))
         
       
    #plotting
    for envList, label in envLists:
        points = []
        for env in envList:
            for p in env[0]:
                print(p)
                #points.append(desd.sph2cart(p))
                points.append(p)
        
        points = np.array(points)
        A = ax.scatter(points[:,0], points[:,1], points[:,2], label=label, zorder=1)
    
    #Circles
    xlin = np.cos(np.linspace(0, 2*np.pi, 100))*rc
    ylin = np.sin(np.linspace(0, 2*np.pi, 100))*rc
    zlin = np.zeros(100)
    ax.plot(xlin, ylin, zlin, color='k', label='rc={}'.format(rc))
    
    ax.legend()
    plt.show()

def varChecks():
    seed = 1
    rc=5
    n=20
    N = 5 #number of atoms in environment
    q=5 #defines number of parameters
    #test.check_chi_var_sum(rc, n=n, atomsNum = N, q=q) #Does the sum of the vars equal the var of the sums? Yes
    #test.check_z_mean(rc,0, 0, n=n, atomsNum = N, q=q, seed=seed) #checks the assumption that E[z*]E[z] = 0? Maybe. when all ms=0 then no.
    #test.check_psi0(rc,0, n=n, atomsNum = N, q=q, seed=seed) #checks the assumption that E[z*]E[z] = 0? Maybe. when all ms=0 then no.
    #test.calc_Eij_var(rc,0, 0, n=n, atomsNum = N, q=q, seed=seed)#
    #test.check_Eij_var(rc, n=n, atomsNum=N, q=q, seed=seed) 
    
    #is E[psi, psi] independant of n and l
    #params1 = [(0,0,0)]
    #params2 = [(np.random.randint(0,5), np.random.randint(0,5), 0) for i in range(10)]
    
    #params1 = [(0,4,0)]
    params1 = [(np.random.randint(0,5), 0, 0) for i in range(3)]
    params2=params1
    #params2 = [(np.random.randint(0,5), np.random.randint(0,5), 0) for i in range(5)]
    
    #If l != 0, does E[psi(n,l,m)] vanish?)
    #params1 = [(0,i,0) for i in range(0, 6)]
    #params2 = [(0,0,0)]
    
    #params1 = [(0,4,i) for i in range(-4, 5)]
    #:w
    #params1 = [(0,4,4), (0,4,0), (0,4,-4)]
    #params2 = [(0,0,0)]
    test.check_psi0(rc, params1, params2, n=np.logspace(np.log10(10), np.log10(1e3), 10))

def paramRange():
    global n, maxp
    maxparam = np.inf 
    Nb = n
    if maxp == 'max': maxp = Nb**2
    elif maxp == 'half': maxp = Nb
    elif maxp == 'min': maxp = Nb**2; maxparam = int((3*Nb**2)*(Nb-1))
    else: maxp = int(maxp)
    paramNames = ['q', 'r', 's', 't', 'u', 'v', 'p']
    tupNames = ['n0', 'n1', 'n2', 'l0', 'l1', 'l2', 'p']
    
    for q in range(7):
        paramRanges = [set() for i in range(7)] #qrstuvp
        tupRanges = [set() for i in range(7)] #n0, n1, n2, l0, l1, l2, p
        paramList = desd.get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=True)
        for param, tup in paramList:
            for i in range(7):
                paramRanges[i].add(param[i])
                tupRanges[i].add(tup[i])
        print('==== q: {} ====='.format( q))
        for i in range(7):
            print('{}: {}'.format(paramNames[i], paramRanges[i]))
        for i in range(7):
            print('{}: {}'.format(tupNames[i], tupRanges[i]))

#def saveScoreDicts(outfn, scoreDicts, plotTitle):
#    global jobId, outDir, n, q, maxp, atomsNum 
#    distType = args.d #Distance comparison metric. L1 L2 0
#    compareMetric = args.c #L1, L2, GW, G2, G3, SOAP
#
#    outFile = open(outfn, 'w')
#    outFile.write(plotTitle+'\n')
#    #outFile.write('#jobId,n,q,maxp,atomsNum,distType,compareMetric\n')
#    #outFile.write('{},{},{},{},{},{},{}\n'.format(jobId,n,q,maxp,atomsNum,distType,compareMetric))
#    for i in range(len(scoreDicts)):
#        data = scoreDicts[i]
#        scoreDict = data[0]
#        nparams = data[1]
#        Ne = data[2]
#        label = data[3]
#        
#        outFile.write('scoreDict,{}\n'.format(i))
#        outFile.write('#label, Ne, nparams\n')
#        outFile.write('"{}",{},{}\n'.format(label, Ne, nparams))
#        outFile.write('#i,j,des,compare\n'.format(label, Ne, nparams))
#        for key, tup in scoreDict.items():
#            i,j = key
#            tup = list(tup)
#            des, compare = tup
#            outFile.write('{},{},{},{}\n'.format(i,j,des,compare))
#    outFile.close()
#
#def saveErrDicts(outfn, plotDict, plotTitle):
#    global jobId, outDir, n, q, maxp, atomsNum 
#    distType = args.d #Distance comparison metric. L1 L2 0
#    compareMetric = args.c #L1, L2, GW, G2, G3, SOAP
#
#    outFile = open(outfn, 'w')
#    outFile.write(plotTitle+'\n')
#    #outFile.write('#jobId,n,q,maxp,atomsNum,distType,compareMetric\n')
#    #outFile.write('{},{},{},{},{},{},{}\n'.format(jobId,n,q,maxp,atomsNum,distType,compareMetric))
#        
#        
#    for q, tup in plotDict.items():
#        maxparamList, stdList, normstdList, normstdsqrtList= tup
#        outFile.write('q,{}\n'.format(q))
#        outFile.write('#maxparam,std,normstd,normstdsqrt\n')
#        for i in range(len(maxparamList)):
#            maxparam = maxparamList[i]
#            std = stdList[i]
#            normstd = normstdList[i]
#            normstdsqrt = normstdsqrtList[i]
#            outFile.write('{},{},{},{}\n'.format(maxparam,std,normstd,normstdsqrt))
#    outFile.close()

def doDesError(genType = 'lattice', byParams = True):
    rc = 2
    global n, atomsNum
    
    rcenv = 1
    offset=[0,0,0]
    seed=args.seed
    envseed = args.envseed
    print('n: {}'.format(n))
    print('seed:', seed)
    print('genType:', genType)

    if not isinstance(args.std, type(None)): std = args.std
    else: std = 0.005 
    latticeCoeff=[0.8,0.8,1.5]
    if genType.endswith('Ref'): 
        if not isinstance(args.std, type(None)): std = args.std[0]
        else: std = 0.005
    ref = None 
    #ref = 0 

    distType = args.d #Distance comparison metric. L1 L2 0
    compareMetric = args.c #L1, L2, GW, G2, G3, SOAP
    actions = {}
    print('Descriptor Metric: {}, Comparison Metric: {}, Ref: {}'.format(distType, compareMetric, ref))
   
    envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, envseed = envseed, offset = offset, genType=genType, std=std, sph=True, useLocalSeed=True, **actions)

    #maxparams = np.floor(np.linspace(10, 100, 10))

    scoreDicts = []



    if byParams:
        maxp = Nb**2
        #maxparams = np.floor(np.linspace(10, 1000, 50))
        #maxparams = np.floor(np.linspace(10, 200, 100))
        maxparams = np.floor(np.linspace(3*Nb**2*(Nb-1)*0.5, 3*Nb**2*(Nb-1)*1.5, args.num))
        if not isinstance(args.lo, type(None)) and not isinstance(args.hi, type(None)):
            maxparams = np.floor(np.linspace(args.lo, args.hi, args.num))
        else:
            maxparams = np.floor(np.linspace(3*Nb**2*(Nb-1)*0.5, 3*Nb**2*(Nb-1)*1.5, args.num))

        if args.short: #Run all the heavy calculations once and then chop them up for each parameter
            maxparam = maxparams[-1]
            q = test.minq_param(Nb, maxp=maxp, maxparam=maxparam)
            paramListFull = desd.get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=False)
            if distType.lower() in ['zeros', 'zeros', '0']:
                print('Replacing descriptors with all zeros for diagnostics.')
                desListFull = [np.zeros(1) for i in range(len(envList))]
            else:
                desListFull = test.gen_descriptors(rc, envList, q, Ne,Nb, maxp=maxp, maxparam=maxparam, multiprocessing=mp, cores=cores, cleanMP=False, explicit=False, doPlot=False)
            desArgs = [rc, envList, q, Ne,Nb]
            desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}
            #Run the comparisons once and then fold them into later scoreDicts
            scoreDictFull = test.compare_env(rc, envList, desListFull, distType=distType,compareMetric=compareMetric, doPlot=False, ref=ref, compareDesArgs= desArgs, compareDesKwargs = desKwargs)

    
        for maxparam in maxparams:
            #Generate Parameters
            
            print('maxparam:', maxparam)
            q = test.minq_param(Nb, maxp=maxp, maxparam=maxparam)
            if args.short:
                paramList = paramListFull[0:int(maxparam)]
                desList = [des[0:int(maxparam)] for des in desListFull]
                desArgs = [rc, envList, q, Ne,Nb]
                desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}
                scoreDict = test.compare_env(rc, envList, desList, distType=distType,compareMetric='L2', doPlot=False, ref=ref, compareDesArgs= desArgs, compareDesKwargs = desKwargs)
                scoreDict = {key:(scoreDict[key][0], val[1]) for key, val in scoreDictFull.items()}

            else:
                paramList = desd.get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=False)
                print('q: {}, 1 < p < {}, no parameters: {} > {}, Ne={}, Nb={}'.format(q, maxp, len(paramList), 3*Nb**2*(Nb-1), Ne, Nb))
                if distType.lower() in ['zeros', 'zeros', '0']:
                    print('Replacing descriptors with all zeros for diagnostics.')
                    desList = [np.zeros(1) for i in range(len(envList))]
                else:
                    desList = test.gen_descriptors(rc, envList, q, Ne,Nb, maxp=maxp, maxparam=maxparam, multiprocessing=mp, cores=cores, cleanMP=False, explicit=False, doPlot=False)
            
                desArgs = [rc, envList, q, Ne,Nb]
                desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}
                scoreDict = test.compare_env(rc, envList, desList, distType=distType,compareMetric=compareMetric, doPlot=False, ref=ref, compareDesArgs= desArgs, compareDesKwargs = desKwargs)
            
            scoreDicts.append({'scores':scoreDict, 'nparams':len(paramList), 'Ne':Ne,'Nb':Nb, 'q':q})
    else:
        if not isinstance(args.lo, type(None)) and not isinstance(args.hi, type(None)):
            qList = np.arange(args.lo, args.hi, args.step)
        
        else:
            qList = np.arange(1, 6)
        #qList = np.arange(1, 4)
        maxps = np.arange(1, Nb**2)
        maxparam=np.inf
        maxparams = [np.inf, -np.inf]
        for q in qList:
            for maxp in maxps:
                #Generate Parameters
                
                print('q:', q, 'maxp:', maxp)
                #q = test.minq_param(Nb, maxparam)
                paramList = desd.get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=False)
                maxparams[0] = min(maxparams[0], len(paramList))
                maxparams[1] = max(maxparams[1], len(paramList))
                print('q: {}, 1 < p < {}, no parameters: {} > {}, Ne={}, Nb={}'.format(q, maxp, len(paramList), 3*Nb**2*(Nb-1), Ne, Nb))
                if distType.lower() in ['zeros', 'zeros', '0']:
                    print('Replacing descriptors with all zeros for diagnostics.')
                    desList = [np.zeros(1) for i in range(len(envList))]
                else:
                    desList = test.gen_descriptors(rc, envList, q, Ne,Nb, maxp=maxp, maxparam=maxparam, multiprocessing=mp, cores=cores, cleanMP=False, explicit=False, doPlot=False)
                
                desArgs = [rc, envList, q, Ne,Nb]
                desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}

                scoreDict = test.compare_env(rc, envList, desList, distType=distType,compareMetric=compareMetric, doPlot=False, ref=ref, compareDesArgs = desArgs, compareDesKwargs = desKwargs)
                scoreDicts.append({'scores':scoreDict, 'nparams':len(paramList), 'Ne':Ne,'Nb':Nb, 'q':q})
    


    infoDict = {
            'rc':rc, 
            'n':n, 
            'atomsNum' :atomsNum, 
            'q'       :q, 
            'maxp'    :maxp, 
            'rcenv'   :rcenv, 
            'offset'  :offset,
            'genType' :genType, 
            'seed'    :seed, 
            'envseed' :envseed, 
            'std'     :std, 
            'distType':distType, 
            'compareMetric':compareMetric,
            }

    reload(matplotlib)
    matplotlib.use(defaultBack)
    reload(plt)

    fig, axs = plt.subplots(1,2, figsize=(15, 10))
    fig2, ax3 = plt.subplots(1,1, figsize=(10, 10))
    ax = axs[0]
    axparam = ax.twinx() #Should be invisible, mostly used for positioning
    axparam.get_yaxis().set_visible(False)
    ax1 = axs[1]
    ax2 = ax1.twinx()
    ax1param = ax1.twinx() #Should be invisible, mostly used for positioning
    ax1param.get_yaxis().set_visible(False)
    #For the parameters part
    maxq = 0
    plotDict = {} 
    scoreDictsTemp = {}
    for k, dat in enumerate(scoreDicts):
        scoreDict= dat['scores']  
        nparams= dat['nparams']  
        Ne= dat['Ne']  
        q= dat['q']  
        scoreDictsTemp[k] = dat 
        #Dealing with parameters
        maxq = max(maxq, q)            
                
        scoreArray = np.array([val for val in scoreDict.values()]) 
        scorestd = np.std(scoreArray)
        scorenormstd = np.std(scoreArray/nparams) #NORMALIZATION HAPPENS HERE
        scorenormstdsqrt = np.std(scoreArray/np.sqrt(nparams)) #NORMALIZATION HAPPENS HERE
        if q not in plotDict.keys():
            plotDict[q] =  {}
            plotDict[q]['nparams'] = []
            plotDict[q]['scorestd'] = []
            plotDict[q]['scorenormstd'] = []
            plotDict[q]['scorenormstdsqrt'] = []
        plotDict[q]['nparams'].append(nparams)
        plotDict[q]['scorestd'].append(scorestd)
        plotDict[q]['scorenormstd'].append(scorenormstd)
        plotDict[q]['scorenormstdsqrt'].append(scorenormstdsqrt)

    fig, ax, handles1 = pltu.errPlot(plotDict, infoDict = infoDict, scoreDicts = scoreDictsTemp, fig = fig, ax=ax, normType='std', returnHandles= True, sufficientLine = True, doParamRanges = True)
    handles2 = []
    fig, ax1, handles = pltu.errPlot(plotDict, infoDict = infoDict, scoreDicts = scoreDictsTemp, fig = fig, ax=ax1, normType='normstd', returnHandles= True, sufficientLine = True, doParamRanges = True, colorbar=False, marker='^', zorder=2, linestyle='-')
    handles2.extend(handles)
    fig, ax2, handles = pltu.errPlot(plotDict, infoDict = infoDict, scoreDicts = scoreDictsTemp, fig = fig, ax=ax2, normType='normstdsqrt', returnHandles= True, sufficientLine = True, doParamRanges = False, marker='s', zorder=2, linestyle = '--')
    handles2.extend(handles)
   
    fig2, ax3 = pltu.smokePlot(scoreDictsTemp, infoDict=infoDict, fig=fig2, ax = ax3, colormap = 'viridis', colorbar=True, normalize = True, sqrtNorm=True, refs=args.refs, slopeDiv = min(4, len(scoreDictsTemp)))
   # handles1 = []
   # handles2 = []
   # for q, dat in plotDict.items():
   #     maxparamList    = dat['nparams']
   #     stdList         = dat['scorestd']
   #     normstdList     = dat['scorenormstd']
   #     normstdsqrtList = dat['scorenormstdsqrt']
   #     #print(maxparamList, stdList, normstdList)
   #     A = ax.plot(maxparamList, stdList, label = 'q: {}'.format(q), markersize=5, marker='o', linestyle = '-', zorder=2)
   #     B = ax1.plot(maxparamList, normstdList,label = 'q: {}, dist/dim'.format(q),  color = A[0].get_color(), marker='^', markersize=5, linestyle = '-', zorder=2)
   #     C = ax2.plot(maxparamList, normstdsqrtList,label = 'q: {}, dist/sqrt(dim)'.format(q),  color = A[0].get_color(), marker='s', markersize=5, linestyle = '--', zorder=2)
   #     handles1.append(A[0])
   #     handles2.append(B[0])
   #     handles2.append(C[0])


   # nameList = ['n0', 'n1', 'n2', 'l0', 'l1', 'l2', 'p']
   # paramHandlesDict = {}
   # maxparamList = []
   # for val in range(int(maxparams[0]), int(maxparams[-1])):
   #     maxparamList.append(val)
   #     maxparamList.append(val+0.9)
   # maxparamList = np.array(maxparamList)
   # paramList = desd.get_parameters(maxq, Nb, maxp=maxp, maxparam=maxparams[-1], lexiSort=True)
   # upperParams = np.zeros(7)-np.inf
   # lowerParams = np.zeros(7)+np.inf
   # for tup, param in paramList:
   #     for i in range(7):
   #         upperParams[i] = max(upperParams[i], param[i]) #get the max q's of each
   #         lowerParams[i] = min(lowerParams[i], param[i]) #get the max q's of each
   # print(len(paramList[int(maxparams[0]):int(maxparams[-1])]), maxparams)
   #
   # normP = pltcolors.Normalize(vmin=np.min(lowerParams), vmax=np.max(upperParams))
   # colormapScalarP = pltcm.ScalarMappable(norm=normP)
   # colormapScalarP.set_array([])
   # colormapP = colormapScalarP.get_cmap()
   # if np.max(upperParams[0:6]) <= 10: 
   #     cmap = 'tab10'
   #     norm=pltcolors.Normalize(vmin=0, vmax=10)
   #     colormapScalar = pltcm.ScalarMappable(cmap = cmap, norm=norm)
   # elif np.max(upperParams[0:6]) <= 20: 
   #     cmap = 'tab20'
   #     norm=pltcolors.Normalize(vmin=0, vmax=20)
   #     colormapScalar = pltcm.ScalarMappable(cmap = cmap, norm=norm)
   # else: 
   #     norm = normP
   #     colormapScalar = colormapScalarP
   # colormap = colormapScalar.get_cmap()
   # #colorlist = list(pltcolors.TABLEAU_COLORS)
   # for i in range(7):
   #     for val in range(int(lowerParams[i]), int(upperParams[i])+1):
   #         if i >= 6: facecolor = colormapP(normP(val))
   #         else: facecolor = colormap(norm(val))
   #         where=[]
   #         #print(i, val, facecolor)
   #         for k, data in enumerate(paramList[int(maxparams[0]):int(maxparams[-1])]):
   #             tup, param = data
   #             #print(i, param, param[i], val, param[i] == val)
   #             if param[i] == val:
   #                 where.append(True)
   #                 where.append(True) #twice b/c half steps
   #             else:
   #                 where.append(False)
   #                 where.append(False) #twice b/c half steps
   #         if val < 0: hatch = '--'
   #         else: hatch = None
   #         collect = pltcol.BrokenBarHCollection.span_where(
   #                 maxparamList, 
   #                 ymin=i,
   #                 ymax=i+0.9,
   #                 where=where,
   #                 facecolor=facecolor,
   #                 hatch=hatch,
   #                 alpha = 0.5,
   #                 label = val,
   #                 zorder=0,
   #                 )
   #         axparam.add_collection(collect)
   #         #axparam.scatter(maxparamList[where], np.zeros(len(maxparamList[where]))+i, color=facecolor)
   #         #Repeated twice b/c can't reshare artists
   #         collect = pltcol.BrokenBarHCollection.span_where(
   #                 maxparamList, 
   #                 ymin=i,
   #                 ymax=i+0.9,
   #                 where=where,
   #                 facecolor=facecolor,
   #                 hatch=hatch,
   #                 label = val,
   #                 zorder=0,
   #                 alpha=0.5
   #                 )
   #         ax1param.add_collection(collect)
   #         if val not in paramHandlesDict and i < 6: paramHandlesDict[val] = collect
   #     axparam.text(maxparamList[0], i, '{}'.format(nameList[i]), fontsize='large', color='white')
   #     ax1param.text(maxparamList[0], i, '{}'.format(nameList[i]), fontsize='large', color='white')
   # print(maxparams)
    plotTitle = 'rc={}, no. env={}, atoms/env={}, q={}, maxp={}\n gen. rc={}, offset={}, genType={}, seed={}, envseed={}, std={}\ndistType={}, compare={}'.format(rc, n, atomsNum, q, maxp, rcenv, offset,genType, seed, envseed, std, distType, compareMetric)
    fig.suptitle(plotTitle, fontsize = 'medium')
    fig2.suptitle(plotTitle, fontsize = 'medium')
    

    ax.set_xlabel('Number of Dimensions')
    ax1.set_xlabel('Number of Dimensions')
    ax.set_ylabel('Std of Des. Dist')
    ax1.set_ylabel('Std of Des. Dist / (dim) [▲]')
    ax2.set_ylabel('Std of Des. Dist / '+ r'$\sqrt{dim}$ ' + '[■]')
    axparam.set_ylim(0, 30)
    ax1param.set_ylim(0, 30)
    C = ax.axvline(3*Nb**2*(Nb-1), label=r'Conjecture 2.3, sufficient $\xi_p$', linestyle=':', color='k',) 
    ax1.axvline(3*Nb**2*(Nb-1), label=r'Conjecture 2.3, sufficient $\xi_p$', linestyle=':', color='k',) 
    handles1.append(C)
    ax.legend(handles=handles1, loc='upper center')
    #ax1.legend(handles=handles2, loc='upper center')

    #if np.max(upperParams[0:6])<=20:   
    #    ax.legend(handles=list(paramHandlesDict.values()))
    #fig.colorbar(colormapScalarP, ax=ax) 
    if byParams:paramsStr = 'params'
    else :paramsStr = 'p'


    try: os.mkdir('{}/images'.format(outDir))
    except: pass
    savefn = '{}/images/{}-{}-err.png'.format(outDir, jobId, paramsStr)
    savesmokefn = '{}/images/{}-{}-err-smoke.png'.format(outDir, jobId, paramsStr)
    outfn = '{}/images/{}-{}-err.json'.format(outDir, jobId, paramsStr)
    #saveErrDicts(outfn, plotDict plotTitle)
    if args.save:
        pltu.saveRunJson(outfn, scoreDicts, infoDict = infoDict, errDict = plotDict)
    fig.savefig(savefn, dpi=300)
    fig2.savefig(savesmokefn, dpi=300)
    if args.i:
        plt.show()


def doDes(genType = 'lattice', plotOnly=False):
    rc=2
    #n=100
    #q=2
   
    global n,q, maxp, atomsNum

    rcenv = 1
    offset=[0,0,0]
    #genType = '2RefLineX' #Most important setting
    seed=args.seed
    envseed = args.envseed
    #seed = 3104827254
    print('n: {}, q: {}, p: {}'.format(n, q, maxp))
    print('seed:', seed)
    print('genType:', genType)

    #if genType in ['lattice', 'peturb', 'counter']: stds = [0]; latticeCoeff=[0.8,0.8,1.5]
    if not isinstance(args.std, type(None)): stds = args.std
    else: stds = [0.1, 0.05, 0.01, 0.005]
    #stds = [0, 0, 0, 0]
    latticeCoeff=[0.8,0.8,1.5]
    if genType.endswith('Ref'): 
        if not isinstance(args.std, type(None)): std = args.std[0]
        else: std = 0.005
    else: std = 0


    xLineDist = False #Replaces yaxis (usually GW dist) with distance between moving points 
    revXY = False #Swaps x and y axis
    refs = args.refs
    #refs = None 
    #refs = [0, int(np.floor(n*0.25)), int(np.floor(n*0.5)), int(np.floor(n*0.75)), n-1] #ALSO IMPORTANT
    #refs = [0,  int(np.floor(n*0.5)),  n-1] #ALSO IMPORTANT
    #refs = [0] 

    distType = args.d #Distance comparison metric. L1 L2 0
    compareMetric = args.c #L1, L2, GW, G2, G3, SOAP
    plotType = 'hist' #scatter, hist
    print('Descriptor Metric: {}, Comparison Metric: {}, Plot Type: {}, Refs: {}'.format(distType, compareMetric, plotType, refs))

    scoreDicts = []
    envLists = []
    minNb = np.inf
    maxNb = 0
   
    actions = {}
    #actions = {'rotateFull':[0,0,0]}
    #actions = {'rotate':[np.pi/4, 0, 0]}

    if genType in ['lattice', 'peturb'] or (genType.startswith('counter') and not genType.endswith('Ref')):
        for std in stds:
            print('std:', std)
            addRef = True
            envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, envseed = envseed, offset = offset, genType=genType, latticeCoeff = latticeCoeff, std=std, sph=True, center=True, posLattice=False, addRef=addRef, **actions)
            envLists.append((envList, Ne, Nb, 'std: {},'.format(std)))
            if Nb < minNb: minNb = Nb #So that it's consistent between runs.
            if Nb > maxNb: maxNb = Nb
    elif genType.startswith('counter') and genType.endswith('Ref'):
        if isinstance(args.envseed, type(None)):
            print('Setting envseed to 1 to ensure degenerate environments')
            envseed = 1
        addRef = True
        print('std:', std)
        temp = [(genType, True, 'X+ vs X-, std={}, '.format(std)),
                (genType, False, 'X- vs X+, std={}, '.format(std)),
                (genType.replace('Ref', ''), True, 'X+ vs X+, std={}, '.format(std)),
                (genType.replace('Ref', ''), False, 'X- vs X-, std={}, '.format(std)),]
        for g, dt, st in temp:
            envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, envseed = envseed, offset = offset, genType=g, std=std, sph=True, center=True, addRef=addRef, useLocalSeed=False, degenType=dt, **actions) # X+ vs X-
            envLists.append((envList, Ne, Nb, st))
            if Nb < minNb: minNb = Nb #So that it's consistent between runs.
            if Nb > maxNb: maxNb = Nb
            
        refs = [0] #KIND OF IMPORTANT
    else:
        if not isinstance(refs, type(None)) and len(refs) > 0: addRef = True
        else: addRef = False #Forces the first environment to not be peturbed
        envList, Ne, Nb = test.gen_env_list(rcenv, n, atomsNum, seed=seed, envseed = envseed, offset = offset, genType=genType, std=stds[0], sph=True, useLocalSeed=True, addRef = addRef)
        envLists.append((envList, Ne, Nb, ''), **actions) 
        if Nb < minNb: minNb = Nb #So that it's consistent between runs.
        if Nb > maxNb: maxNb = Nb
           

    infoDict = {
            'rc':rc, 
            'n':n, 
            'atomsNum' :atomsNum, 
            'q'       :q, 
            'maxp'    :maxp, 
            'rcenv'   :rcenv, 
            'offset'  :offset,
            'genType' :genType, 
            'seed'    :seed, 
            'envseed' :envseed, 
            'std'     :std, 
            'distType':distType, 
            'compareMetric':compareMetric,
            }


    #Check Conjecture 2.3. Minimum number of parameters should be 3*N**2(N-1)
    #maxparam = 3*Nb**2(Nb-1)
    #maxparam=np.inf
    maxparam=np.inf
    if maxp == 'max': maxp = minNb**2
    elif maxp == 'half': maxp = minNb
    elif maxp == 'min': maxp = minNb**2; maxparam = int((3*Nb**2)*(Nb-1))
    else: maxp = int(maxp)
    desRange=np.array([np.inf, -np.inf])
    compRange=np.array([np.inf, -np.inf])


    for envList, Ne, Nb, label in envLists:
        #Q defined in section 3
        #minq = test.minq(6)
        minq = test.minq_param(Nb, maxp=maxp, maxparam= 3*Nb**2*(Nb-1))
        if minq > q and not args.nosub:
            print('Selected q is too low! Substituting with q={}'.format(minq))
            q = minq
        #minq = 5#test.minq(6)
        #1 < p < Nb**2
        if not plotOnly:
            paramList = desd.get_parameters(q, Nb, maxp=maxp, maxparam=maxparam, lexiSort=True)
            print('q: {}, 1 < p < {}, no parameters: {} > {}, Ne={}, Nb={}'.format(minq, maxp, len(paramList), 3*Nb**2*(Nb-1), Ne, Nb))
            if distType.lower() in ['zeros', 'zeros', '0']:
                print('Replacing descriptors with all zeros for diagnostics.')
                desList = [np.zeros(1) for i in range(len(envList))]
            else:
                desList = test.gen_descriptors(rc, envList, q, Ne,Nb, maxp=maxp, maxparam=maxparam, multiprocessing=mp, cores=cores, cleanMP=False, explicit=False, doPlot=False)


        if not isinstance(refs, type(None)):
            for ref in refs:
                refpoints = []
                env = envList[ref]
                for p in env[0]:
                    refpoints.append(desd.sph2cart(p))
                refpoints = np.array(refpoints)
                
                if not plotOnly:
                    desArgs = [rc, envList, q, Ne,Nb]
                    desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}
                    scoreDict = test.compare_env(rc, envList, desList, distType=distType,compareMetric=compareMetric, doPlot=False, ref=ref, compareDesArgs = desArgs, compareDesKwargs=desKwargs)
                    desScores = [tup[0] for tup in scoreDict.values()]
                    if max(desScores) > desRange[1]: desRange[1] = max(desScores)
                    if min(desScores) < desRange[0]: desRange[0] = min(desScores)
                    compScores = [tup[1] for tup in scoreDict.values()]
                    if max(compScores) > compRange[1]: compRange[1] = max(compScores)
                    if min(compScores) < compRange[0]: compRange[0] = min(compScores)
                    
                    scoreDicts.append({'scores':scoreDict, 'nparams':len(paramList), 'Ne':Ne,'Nb':Nb, 'label':label+'Ref Env {},'.format(ref), 'q':q })
                
        else:
            if not plotOnly:
                desArgs = [rc, envList, q, Ne,Nb]
                desKwargs= {'maxp':maxp, 'maxparam':maxparam, 'multiprocessing':mp, 'cores':cores, 'cleanMP':False, 'explicit':False, 'doPlot':False}
                scoreDict = test.compare_env(rc, envList, desList, distType=distType, compareMetric=compareMetric, doPlot=False, compareDesArgs = desArgs, compareDesKwargs =desKwargs)
                desScores = [tup[0] for tup in scoreDict.values()]
                if max(desScores) > desRange[1]: desRange[1] = max(desScores)
                if min(desScores) < desRange[0]: desRange[0] = min(desScores)
                compScores = [tup[1] for tup in scoreDict.values()]
                if max(compScores) > compRange[1]: compRange[1] = max(compScores)
                if min(compScores) < compRange[0]: compRange[0] = min(compScores)

                scoreDicts.append({'scores':scoreDict, 'nparams':len(paramList), 'Ne':Ne, 'Nb':Nb, 'label':label, 'q':q})

    #The pyxetl_ff has some matplotlib stuff that messes with the front end stuff so reloading it all is necessary
    reload(matplotlib)
    matplotlib.use(defaultBack)
    reload(plt)

    if not plotOnly:
        fig = plt.figure(figsize=(12,6))
        gs = pltgs.GridSpec(2,2,figure=fig)
        ax = fig.add_subplot(gs[0,0]) 
        ax1 = fig.add_subplot(gs[1,0])
        ax2 = fig.add_subplot(gs[:,1],projection='3d') 
    else:
        fig = plt.figure(figsize=(12,6))
        ax2 = fig.add_subplot(projection='3d') 
    
    ax2.set_xlim(-rc*1.1, rc*1.1)
    ax2.set_ylim(-rc*1.1, rc*1.1)
    ax2.set_zlim(-rc*1.1, rc*1.1)
    #ax2.set_xlim(-0.6, 0.6)
    #ax2.set_ylim(-0.6, 0.6)
    #ax2.set_zlim(-0.6, 0.6)
    #plot rc boundary
    xlin = np.cos(np.linspace(0, 2*np.pi, 100))*rc
    ylin = np.sin(np.linspace(0, 2*np.pi, 100))*rc
    zlin = np.zeros(100)
    ax2.plot(xlin, ylin, zlin, color='k', label='rc={}'.format(rc))

    for i,  dat in enumerate(envLists):
        envList, Ne, Nb, label =dat
        points = []
        for env in envList:
            for p in env[0]:
                points.append(desd.sph2cart(p))
        
        points = np.array(points)
        A = ax2.scatter(points[:,0], points[:,1], points[:,2], label=label, zorder=1)
        if not isinstance(refs, type(None)):
            for j, ref in enumerate(refs):
                refpoints = []
                env = envList[ref]
                for p in env[0]:
                    refpoints.append(desd.sph2cart(p))
                refpoints = np.array(refpoints)
                A = ax2.scatter(refpoints[:,0], refpoints[:,1], refpoints[:,2], s=40, alpha = 1, zorder = 2,label='Ref Env {}'.format(ref))
                if not plotOnly:
                    scoreDicts[int(i*len(refs))+j]['color'] = A.get_facecolors()
        else:
            if not plotOnly:
                scoreDicts[i]['color'] = A.get_facecolors()


    normstr = ''
    if not plotOnly: 
        scoreDictsTemp = {}
        plotDict = {} 
        for k, dat in enumerate(scoreDicts):
            scoreDict= dat['scores']  
            nparams= dat['nparams']  
            Ne= dat['Ne']  
            q= dat['q']
            dat['label'] = '{} params={}, Ne={}'.format(dat['label'], nparams, Ne)
            scoreDictsTemp[k] = dat
            scoreArray = np.array([val for val in scoreDict.values()]) 
            scorestd = np.std(scoreArray)
            scorenormstd = np.std(scoreArray/nparams) #NORMALIZATION HAPPENS HERE
            scorenormstdsqrt = np.std(scoreArray/np.sqrt(nparams)) #NORMALIZATION HAPPENS HERE
            if q not in plotDict.keys():
                plotDict[q] =  {}
                plotDict[q]['nparams'] = []
                plotDict[q]['scorestd'] = []
                plotDict[q]['scorenormstd'] = []
                plotDict[q]['scorenormstdsqrt'] = []
            plotDict[q]['nparams'].append(nparams)
            plotDict[q]['scorestd'].append(scorestd)
            plotDict[q]['scorenormstd'].append(scorenormstd)
            plotDict[q]['scorenormstdsqrt'].append(scorenormstdsqrt)

        normalize = False
        sqrtNorm = False
        if args.norm in ['params', 'nparams']:
            normalize = True
        elif args.norm in ['sqrt']:
            sqrtNorm= True

        fig, ax  = pltu.smokePlot(scoreDictsTemp, infoDict=infoDict, fig = fig, ax=ax, normalize=normalize, sqrtNorm=sqrtNorm, reverseAxis = revXY)
        fig, ax1 = pltu.histPlot(scoreDictsTemp, infoDict = infoDict, fig = fig, ax=ax1, normalize= normalize, sqrtNorm=sqrtNorm, reverseAxis = revXY)
            #ax1.hist(vals[:,0]/norm, bins=100, range=tuple(desRange/norm), color=facecolor, label='{} params={}, Ne={}'.format(label, nparams, Ne))
        #for k in range(len(scoreDicts)):
        #    scoreDict= scoreDicts[k]['scores']  
        #    nparams= scoreDicts[k]['nparams']  
        #    Ne= scoreDicts[k]['Ne']  
        #    label= scoreDicts[k]['label']  
        #    facecolor = scoreDicts[k]['color']
        #    #print(scoreDict, nparams)
        #    vals = []
        #    for key, tup in scoreDict.items():
        #        tup = list(tup)
        #        envList = envLists[0][0]
        #        ienv = envList[key[0]][0]
        #        jenv = envList[key[1]][0]
        #        if revXY:
        #            des, gw = tup
        #            tup = [gw, des]
        #        if xLineDist and genType in ['line', '2RefLineZ', '2RefLineX']: #replaces the GW distance with the distance between moving points.
        #            #print(ienv, jenv)
        #            dist = jenv[1][0] 
        #            tup[1] = dist

        #        print(key, 'des:', tup[0], 'comp:', tup[1])
        #        for i in range(max(len(ienv), len(jenv))):
        #            if i < len(ienv): pi = list(desd.sph2cart(ienv[i]))
        #            else: pi = ''
        #            if i < len(jenv): pj = list(desd.sph2cart(jenv[i]))
        #            else: pj = ''
        #            print('Point {} 1: {} 2: {}'.format(i, pi, pj))
        #            #print(list(ienv[i]), list(jenv[i]))
        #        vals.append(tup)
        #    vals = np.array(vals)

        #    if args.norm in ['params', 'nparams']:
        #        norm=nparams
        #        normstr = ' / no. params'
        #    elif args.norm in ['sqrt']:
        #        norm = np.sqrt(norm)
        #        normstr = ' / sqrt( no. params )'
        #    else:
        #        norm = 1
        #        normstr = ''

            
            #ax.scatter(vals[:,1], vals[:,0]/np.sqrt(nparams), color=facecolor, label='{} params={}, Ne={}'.format(label, nparams, Ne))
            #ax1.hist(vals[:,0]/np.sqrt(nparams), bins=100, range=tuple(desRange/np.sqrt(nparams)), color=facecolor, label='{} params={}, Ne={}'.format(label, nparams, Ne))
            #ax.scatter(vals[:,1], vals[:,0]/norm, color=facecolor, label='{} params={}, Ne={}'.format(label, nparams, Ne))
            #ax1.hist(vals[:,0]/norm, bins=100, range=tuple(desRange/norm), color=facecolor, label='{} params={}, Ne={}'.format(label, nparams, Ne))

    plotTitle = 'rc={}, no. env={}, atoms/env={}, q={}, maxp={}\n gen. rc={}, offset={}, genType={}, seed={}, envseed={}, zeroRef={}\ndistType={}, compare={}'.format(rc, n, atomsNum, q, maxp, rcenv, offset,genType, seed, envseed, addRef, distType, compareMetric)
    fig.suptitle(plotTitle, fontsize = 'medium')


    if compareMetric == 'GW': xlabel = 'GW Score'
    elif compareMetric in ('L1', 'L2'): xlabel = 'Sum of Displacements ({})'.format(compareMetric)
    elif compareMetric == 'chiReplace': xlabel = 'GW (v. chi)'
    else: xlabel = compareMetric
    #ylabel = 'Descriptor dist ({}) / sqrt(no. params)'.format(distType)
    if compareMetric == 'chiReplace': ylabel = 'Descriptor dist (Chi)'+ normstr
    else: ylabel = 'Descriptor dist ({}){}'.format(distType, normstr)

    if revXY: temp = xlabel; xlabel = ylabel; ylabel = temp
    
    if xLineDist and genType in ['line', '2RefLineZ', '2RefLineX']: xlabel = 'Moving Point Location' #replaces the GW distance with the distance between moving points.
    if not plotOnly:
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        #histogram
        #xlabel = 'Descriptor dist ({}) / sqrt(no. params)'.format(distType)
        xlabel = 'Descriptor dist ({}){}'.format(distType, normstr)
        ylabel = 'Count'
        ax1.set_xlabel(xlabel)
        ax1.set_ylabel(ylabel)
        ax.legend( fontsize='x-small')
        ax1.legend(fontsize='x-small')
        savefn = '{}/images/{}-des.png'.format(outDir, jobId)
        #print(scoreDict)
        outfn = '{}/images/{}-des.json'.format(outDir, jobId)
        
        if args.save:
            pltu.saveRunJson(outfn, scoreDicts, infoDict=infoDict, errDict=plotDict)
        #saveScoreDicts(outfn, scoreDicts, plotTitle)
    else:
        savefn = '{}/images/{}-plot.png'.format(outDir, jobId)
    

    try: os.mkdir('{}/images'.format(outDir))
    except: pass
    ax2.legend(fontsize='xx-small')
    plt.savefig(savefn, dpi=300)
    if args.i:
        plt.show()

def foo(hi, hello=None):
    #raise(OSError)
    return '{}, {}, {}'.format(np.random.random(), hi, hello)
def mpTest():    
    test.startMP(cores=5)
    test.pushJobMP((1, foo, ['how'], {'hello':'are'}), repeat=5)
    test.pushJobMP((2, foo, ['why'], {'hello':'what'}), repeat=5)
    test.pushJobMP('END', repeat='all')
    test.popResQueue()
    test.endMP()


if args.t == 'var': varChecks()
elif args.t == 'mptest': mpTest()
elif args.t == 'rangetest': paramRange()
elif args.t.startswith('errp'):
    tup = args.t.partition('=')
    doDesError(tup[2], byParams=False)
elif args.t.startswith('err'):
    tup = args.t.partition('=')
    doDesError(tup[2], byParams=True)
elif args.t.startswith('plot'):
    tup = args.t.partition('=')
    doDes(tup[2], plotOnly=True)
else: doDes(args.t)
mpu.endMP()
